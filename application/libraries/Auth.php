<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth
{

    function __construct()
    {
        $CI = &get_instance();
        $CI->load->library('session');

        
        // $browser = $_SERVER['HTTP_USER_AGENT'];
        // $browser = strtolower($browser);
        // if (strpos($browser, 'chrome') == false) {
        //     print_r("<center><p style='margin-top:100px;'>Opps!, Aplikasi ini hanya didukung oleh browser <b>chrome</b>. <br/><br/>Silahkan gunakan browser <b>Chrome</b> untuk membuka halaman pada applikasi.</p></center>");
        //     die();
        // }else{
        //     print_r("ok");
        // }
    }

    function isLogin(){
        $CI = &get_instance();
        if ($CI->session->userdata("logged") == null || !$CI->session->userdata('logged')) {
            redirect('login');
            die();
        }else{
            $st = $CI->encryption->decrypt($CI->session->userdata('ul'));
            if ($st != "Anggota" && $st != "Danru" &&  $st != "Chief" && $st != "Admin" && $st != "Manajemen" && $st != "Officer"){
                redirect('login');
                die();
            }
        }
    }

    function isAdmin()
    {
        $this->isLogin();

        $CI = &get_instance();
        if ($CI->encryption->decrypt($CI->session->userdata('ul')) != "Admin") {
            redirect('login');
            die();
        }
    }
    function isChief()
    {
        $this->isLogin();
        $CI = &get_instance();
        if ($CI->encryption->decrypt($CI->session->userdata('ul')) != "Chief") {
            redirect('login');
            die();
        }
    }
    function isDanru()
    {
        $this->isLogin();
        $CI = &get_instance();
        if ($CI->encryption->decrypt($CI->session->userdata('ul')) != "Danru") {
            redirect('login');
            die();
        }
    }

    function isAnggota()
    {
        $this->isLogin();
        $CI = &get_instance();
        if ($CI->encryption->decrypt($CI->session->userdata('ul')) != "Anggota") {
            redirect('login');
            die();
        }
    }

    function isObserver()
    {
        $this->isLogin();
        $CI = &get_instance();
        if ($CI->encryption->decrypt($CI->session->userdata('ul')) != "Manajemen" && $CI->encryption->decrypt($CI->session->userdata('ul')) != "Officer") {
            redirect('login');
            die();
        }
    }

    function goToPage($tab=''){
        $CI = &get_instance();
        if( $CI->session->userdata("logged") == null || !$CI->session->userdata('logged') ){
            
        }else{
            if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Admin'){
                redirect("data/record");
            }else if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Manajemen' || $CI->encryption->decrypt($CI->session->userdata('ul')) == 'Officer'){
                redirect("user/Observer".$tab);

            }else if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Chief'){
                redirect("user/Chief".$tab);

            }else if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Danru'){
                redirect("user/Danru".$tab);
            
            }else if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Anggota'){
                redirect("user/Anggota".$tab);
            }else{
                $CI->session->sess_destroy();
                redirect("login");
            }
        }
    }

    function goToPageReturn(){
        $CI = &get_instance();
        if($CI->session->userdata("logged") != null && $CI->session->userdata('logged')){
            if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Admin'){
                return "data/record";
            }else if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Chief'){
                return "user/Chief#report-tab";

            }else if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Danru'){
                return "user/Danru#report-tab";
            
            }else if($CI->encryption->decrypt($CI->session->userdata('ul')) == 'Anggota'){
                return "user/Anggota#report-tab";

            }else{
                $CI->session->sess_destroy();
                return "login";
            }
        }
    }


}
