<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <style type="text/css">
      *:not(.fas,.far){
        font-family: 'Roboto', sans-serif !important;
      }
     /* .logo{
        -webkit-filter: grayscale(100%);  Safari 6.0 - 9.0 
        filter: grayscale(100%);
      }*/
    </style>
    <style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;  /* this affects the margin in the printer settings */
    }
    </style>
    <title></title>
  </head>
  <body>
    <table style="height: 100vh; width: 100%;">
      <tbody>
        <tr>
          <td class="align-middle text-center">
                <p class="mt-4">
                    <form method="post" action="<?PHP echo base_url()."data/".$controller_name?>/qrcode">
                        <input type="hidden" name="id" value="<?PHP echo $this->encryption->encrypt($location->row()->id_location);?>">
                        <input type="hidden" name="rebuild" value="1">
                        <button type="submit" class="btn buttons-pdf btn-sm sc btn-edit-link text-secondary"><i class="fas fa-user-shield"></i>  Security Check QR Code</button>
                    </form>
                </p>
                <div class="col-md-4 offset-md-4 pb-4 px-5">
                    <h1 style="font-weight: 600"><?PHP echo $location->row()->location_name?></h1>
                    <h4 style="font-weight: 300"><?PHP echo $location->row()->pt_name?></h4>
                    <img class="mr-3 mb-2 mt-3" style="max-width: 100%; max-height: 300px" src="<?PHP echo base_url().'assets/qr/images/'.$location->row()->location_qr_code.'.png'?>">
                    
                    <p class="border-bottom pb-2">P<?PHP echo $location->row()->id_pt?> | L<?PHP echo $location->row()->id_location?></p>
                    <img class="bi me-2 logo" src="<?PHP echo base_url()?>assets/img/kmblogo.png?cache=<?PHP echo Date("ymdhis");?>" height="30">
                </div>
          </td>
        </tr>
      </tbody>
    </table>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>