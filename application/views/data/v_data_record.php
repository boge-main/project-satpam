<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Security Checking Application">
	<meta name="keywords" content="Kujang Mitra Bersama, Security, QR Check">
	<meta name="author" content="Kujang Mitra Bersama">
    <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?PHP echo base_url()?>assets/img/favico/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo base_url()?>assets/img/favico/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo base_url()?>assets/img/favico/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo base_url()?>assets/img/favico/favicon-16x16.png">
	<link rel="manifest" href="<?PHP echo base_url()?>assets/img/favico/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?PHP echo base_url()?>assets/img/favico/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;700&display=swap" rel="stylesheet">

    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css"> -->
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css" /> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css" />
    
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <link href="<?PHP echo base_url()?>assets/css/data/data.css?version=<?PHP echo date('YmdHis')?>" rel="stylesheet" >


    <title>Kujang Mitra Bersama</title>
  </head>
  <body>

  	<?PHP 
  		$af = array(
				"db_field_name" 		=> null, // i: untuk dijadikan field id, baiknya diberi nama field di db, pastikan harus unik
				"html_field_name"		=> null,  // i: akan dijadikan nama kolom isian (nama field di HTML)
				"html_readonly"			=> null,
				"html_init_value"		=> null, // i: untuk set default value di kolom isian
				"html_type_input" 		=> null, //value: text|number|select|textarea|date|email| = https://www.w3schools.com/tags/tag_input.asp
				"html_placeholder"		=> null, // i: placeholder (text before input)
				"html_field_note"		=> null, // i: untuk memberikan catatan kaki pada field form
				"html_max"				=> null, // i: max value, berlaku untuk field date|number
				"html_min"				=> null, // i: min value, berlaku untuk field date|number
				"html_options"			=> null, // i: CI query, data pada select option
				"html_option_value"		=> null, // i: value dari query yang akan dijadikan base value untuk kolom ini
				"html_option_multiple"	=> null, // i: selectionnya bisa multiple value atau enggak
				"html_option_text"		=> null, // i: text yang akan ditampilkan pada option select (max 30 char per attribute)
				"js_cascade_field"		=> null, // i: nama field lain yang akan mempengaruhi selection field ini
				"js_cascade_col"		=> null, // i: nama attribut di field ini yang akan dipengaruhi oleh isian field lain
				"custom_id"				=> null, //i: id field, jika null, id_field akan menggunakan db_field_name
				"custom_class"			=> null, //i: jika ingin menambahkan kelas pada field ini
				"custom_js"				=> null, //i: additional javascript
		);
  	?>

  	<!-- HEADER MENU -->
    <div class="container-fluid our-header border-bottom mb-3">
      <div class="container-fluid">
        <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3">
          
          <div class="col-md-3 ">
            <h4 style="font-weight: 800"><img class="img-logo" src="<?PHP echo base_url()?>assets/img/header-logo.png?cache=<?PHP echo Date("ymdhis");?>"></h4>
          </div>

          <ul class="nav nav-pills col-12 col-md-auto mb-2 justify-content-center mb-md-0">
	            <li class="nav-item border-0">
	            	<a href="<?PHP echo base_url()?>data/record" class="jkhd px-3 nav-link text-center 
	            	<?PHP if($sub_menu != 'pt'){echo 'active';}else{echo 'nonactive';}?> border-0" style="border-radius: 15px 0px 0px 15px;width: 250px;"> 
	            		 Data Patroli & Pengguna
	            	</a>
	            </li>
	            <li class="nav-item border-0">
	            	<a href="<?PHP echo base_url()?>data/pt" style="border-radius: 0px 15px 15px 0px;width: 250px;" class="jkhd nav-link px-3 text-center
	            	<?PHP if($sub_menu == 'pt'){echo 'active';}else{echo 'nonactive';}?> border-0">
	            	Data Perusahaan</a>
	        	</li>
	      </ul>

          <div class="col-md-3 text-end">
          	<a href="<?PHP echo base_url()?>login/logout" class="text-decoration-none">
	            <button type="button" class="btn btn-danger btn-sm me-2"><i class="fas fa-sign-out-alt"></i> Logout</button>
	        </a>
          </div>
        </header>
      </div>
    </div> 

    <div class="container" style="min-height: 80vh">
    	<div class="row">

			<!-- KONTEN -->
			<div class="col-sm-12">

				<?PHP if(!$selected_id){?>
					<div class="col-md-12 py-3 px-3 mb-4 bg-white shadow">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
					              <span class="input-group-text rounded-0" id="basic-addon1"><i class="fas fa-building text-white"></i></span>
					              <select class="form-select text-center rounded-0" type="text" id="select_perusahaan" name="select_perusahaan" style="padding:8px">
					                <?PHP foreach($pt->result() as $ptrow){?>
					                  <option value="<?PHP echo $ptrow->id_pt?>" <?PHP if($ptrow->id_pt == $this->session->userdata('up')){echo "selected";}?>>
					                    <?PHP echo $ptrow->pt_name?>
					                  </option>
					                <?PHP }?>
					              </select>
					            </div>
					        </div>
					        <div class="col-md-6">
					            <ul class="nav nav-pills nav-fill">
								  <li class="nav-item">
								    <a href="<?PHP echo base_url();?>data/record" class="nav-link <?PHP if($sub_menu == 'record'){echo 'active';}else{echo 'link-light';}?>">
								        <i class="far fa-chart-bar"></i>
								        Laporan
								    </a>
								  </li>
								  <li class="nav-item nav-center">
								    <a href="<?PHP echo base_url();?>data/location" class="nav-link <?PHP if($sub_menu == 'location'){echo 'active';}else{echo 'link-light';}?>">
								        <i class="fas fa-qrcode"></i>
								        Titik Patroli
								    </a>
								  </li>
								  <li class="nav-item">
								    <a href="<?PHP echo base_url();?>data/user" class="nav-link <?PHP if($sub_menu == 'user'){echo 'active';}else{echo 'link-light';}?>">
								        <i class="far fa-user"></i>
								        Pengguna
								    </a>
								  </li>
								</ul>
							</div>
						</div>
					</div>
				<?PHP }?>

				<?PHP if(null !== $this->session->flashdata('success')){?>
			        <div class="pt-3">
				        <div class="alert alert-success alert-dismissible fade show" role="alert">
						  <strong>Yey!</strong> <?PHP echo $this->session->flashdata('success');?>
						  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>
					</div>
					<?PHP }else if(null !== $this->session->flashdata('danger')){?>
					<div class="pt-3">
				        <div class="alert alert-danger alert-dismissible fade show" role="alert">
						  <strong>Oops!</strong> <?PHP echo $this->session->flashdata('danger');?>
						  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>
					</div>
					<?PHP }else if(null !== $this->session->flashdata('warning')){?>
					<div class="pt-3">
				        <div class="alert alert-warning alert-dismissible fade show" role="alert">
						  <strong>Warning!</strong> <?PHP echo $this->session->flashdata('warning');?>
						  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>
					</div>
					<?PHP }else if(null !== $this->session->flashdata('info')){?>
					<div class="pt-3">
				        <div class="alert alert-primary alert-dismissible fade show" role="alert">
						  <strong>Info!</strong> <?PHP echo $this->session->flashdata('info');?>
						  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>
					</div>
				<?PHP }?>
				<!-- FORM INPUT -->
				
				<?PHP if($selected_id){ ?>
				<div class="row">

		            <div class="col-md-12 text-center mb-2">Record ID = <?PHP echo $selected_id?></div>
					<div class="col-md-5 mt-3">
				        <div class="<?PHP if(!$selected_id){echo 'collapse';}?>" id="collapseExample">
				          <div class="card card-body rounded border-none">
				          	<form method="post" action="<?PHP echo base_url().'data/'.$controller_name.'/input';?>">
					            <div class="row">
					              <input type="hidden" name="id" value="<?PHP echo $this->encryption->encrypt($selected_id)?>">
					              <?PHP foreach ($form as $key) {
					              		foreach ($af as $k => $v) {
					              			if(!isset($key[$k])){
					              				$key[$k] = $v;
					              			}
					              		}
					              ?>
					              	<?PHP if($key['html_type_input'] == 'split'){ ?>
					              		<div class="col-md-12 mb-2 mt-4">
							                <div class="input-group input-group-sm">
							                  	<h5><?PHP echo $key['html_field_name'];?></h5>
							                </div>
							            </div>
					              	<?PHP }else if($key['html_type_input'] == 'text'){ ?>
					              		<div class="col-md-12 mb-3">
							                <div class="input-group input-group-sm">
							                  <span class="input-group-text"><?PHP echo $key['html_field_name'];?></span>
							                  <input type="text" id="fid-<?PHP echo $key['db_field_name'];?>" placeholder="<?PHP echo $key['html_placeholder']; ?>" name="<?PHP echo $key['db_field_name'];?>" class="form-control" <?PHP if($key['html_required']){echo 'required';}?> <?PHP if($key['html_readonly']){echo 'readonly';}?>
							                  value="<?PHP echo $key['html_init_value'];?>">
							                </div>
							                <?PHP if($key['html_field_note'] !=null){?>
							                	<small class="form-text text-muted"><?PHP echo $key['html_field_note']; ?></small>
							                <?PHP }?>
							            </div>
							        <?PHP }else if($key['html_type_input'] == 'password'){ ?>
					              		<div class="col-md-12 mb-3">
							                <div class="input-group input-group-sm">
							                  <span class="input-group-text"><?PHP echo $key['html_field_name'];?></span>
							                  <input type="password" id="fid-<?PHP echo $key['db_field_name'];?>" placeholder="<?PHP echo $key['html_placeholder']; ?>" name="<?PHP echo $key['db_field_name'];?>" class="form-control" <?PHP if($key['html_required']){echo 'required';}?> <?PHP if($key['html_readonly']){echo 'readonly';}?>
							                  value="<?PHP echo $this->encryption->decrypt($key['html_init_value']);?>">
							                </div>
							                <?PHP if($key['html_field_note'] !=null){?>
							                	<small class="form-text text-muted"><?PHP echo $key['html_field_note']; ?></small>
							                <?PHP }?>
							            </div>
							        <?PHP }else if($key['html_type_input'] == 'number'){ ?>
					              		<div class="col-md-12 mb-3">
							                <div class="input-group input-group-sm">
							                  <span class="input-group-text"><?PHP echo $key['html_field_name'];?></span>
							                  <input type="number" id="fid-<?PHP echo $key['db_field_name'];?>" name="<?PHP echo $key['db_field_name'];?>" class="form-control" placeholder="<?PHP echo $key['html_placeholder']; ?>" <?PHP if($key['html_required']){echo 'required';}?> <?PHP if($key['html_readonly']){echo 'readonly';}?>
							                  value="<?PHP echo $key['html_init_value'];?>">
							                </div>
							                <?PHP if($key['html_field_note'] !=null){?>
							                	<small class="form-text text-muted"><?PHP echo $key['html_field_note']; ?></small>
							                <?PHP }?>
							            </div>
							        <?PHP }else if($key['html_type_input'] == 'select'){ ?>
							        	<div class="col-md-12 mb-3">
							                <div class="input-group input-group-sm">
							                  <span class="input-group-text"><?PHP echo $key['html_field_name']?></span>
							                  <select id="fid-<?PHP echo $key['db_field_name'];?>" name="<?PHP echo $key['db_field_name'];?><?PHP if($key['html_option_multiple'] == true){echo "[]";}?>" class="form-select form-select-sm filter-form" aria-label=".form-select-sm example" <?PHP if($key['html_required']){echo 'required';}?> <?PHP if($key['html_readonly']){echo 'readonly';}?> 
							                  	<?PHP if($key['html_option_multiple'] == true){echo "multiple";}?> >
							                    <?PHP foreach ($key['html_options'] as $row) { $row = (array) $row;
							                    	$str = str_replace(", ",",",$key['html_init_value']);
							                    	$str = str_replace(" ,",",",$str);
							                    	$initvalue = explode(",", $str);
							                    ?>
							                    	

							                    	<option class="cls-opt-<?PHP echo $key['db_field_name'];?>" 
							                    			value="<?PHP echo $row[$key['html_option_value']];?>" 
							                    			<?PHP if(in_array($row[$key['html_option_value']], $initvalue)){echo "selected";}?>
							                    		<?PHP foreach ($row as $k=>$v) {
							                    			echo 'attr-'.$k.'="'.$v.'" ';
							                    		}?>>
							                    		<?PHP $lp = []; 
							                    			foreach($key['html_option_text'] as $hpt){ 
							                    				array_push($lp, $row[$hpt]); 
							                    			} 
							                    			print(implode(" | ", $lp));?>
							                    	</option>
							                    <?PHP }?>
							                  </select>
							                </div>
							                <?PHP if($key['html_field_note'] !=null){?>
							                	<small class="form-text text-muted"><?PHP echo $key['html_field_note']; ?></small>
							                <?PHP }?>
							            </div>
							        <?PHP }else if($key['html_type_input'] == 'textarea'){ ?>
					              		<div class="col-md-12 mb-3">
							                <div class="input-group input-group-sm">
							                  <span class="input-group-text"><?PHP echo $key['html_field_name'];?></span>
							                  <textarea id="fid-<?PHP echo $key['db_field_name'];?>" placeholder="<?PHP echo $key['html_placeholder']; ?>" name="<?PHP echo $key['db_field_name'];?>" class="form-control" <?PHP if($key['html_required']){echo 'required';}?> <?PHP if($key['html_readonly']){echo 'readonly';}?>
							                  value="<?PHP echo $key['html_init_value'];?>" rows="4" cols="50"><?PHP echo $key['html_init_value'];?></textarea>
							                </div>
							                <?PHP if($key['html_field_note'] !=null){?>
							                	<small class="form-text text-muted"><?PHP echo $key['html_field_note']; ?></small>
							                <?PHP }?>
							            </div>
							        <?PHP }}?>
					              <div class="col-md-12 mb-3 text-end">
					                <button type="submit"class="btn btn-sm fc text-white">Simpan</button>
					        </form>
					                <a href="<?PHP echo base_url().'data/'.$controller_name?>" class="btn btn-sm fourth-gradient text-white">Batal</a>
					              </div>
					        </div>
				        </div>
				    </div>
				</div>
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-6 text-center mt-3" >
							<?PHP if($getimage != null){?>
			                    <?php if ($getimage->num_rows()==1){?>
			                        <img class="w-100 border shadow rounded" src="<?php echo $getimage->row()->image; ?>"/>
			                    <?PHP  }else{?>
			                        <p class="text-center">No Image</p>
			                    <?PHP  } ?>
			                <?PHP }?> 
		                </div> 
		                <div class="col-md-6 mt-3">
					    	<div class="row">
						    	<div class="col-md-6">
						    		<?PHP if($getdocs->row()->img1 !=''){?>
						    			<img class="w-100 shadow rounded" src="<?PHP echo base_url()?>assets/pictures/<?PHP echo $getdocs->row()->img1;?>"/>
						    		<?PHP }else{?>
						    			<img class="w-100  shadow rounded" src="<?PHP echo base_url()?>assets/img/noimage.png"/>
						    		<?PHP }?>
						    	</div>
						    	<div class="col-md-6">
						    		<?PHP if($getdocs->row()->img2 !=''){?>
						    			<img class="w-100  shadow rounded" src="<?PHP echo base_url()?>assets/pictures/<?PHP echo $getdocs->row()->img2;?>"/>
						    		<?PHP }else{?>
						    			<img class="w-100  shadow rounded" src="<?PHP echo base_url()?>assets/img/noimage.png"/>
						    		<?PHP }?>
						    	</div>
						    	<div class="col-md-6 mt-3">
						    		<?PHP if($getdocs->row()->img3 !=''){?>
						    			<img class="w-100  shadow rounded" src="<?PHP echo base_url()?>assets/pictures/<?PHP echo $getdocs->row()->img3;?>"/>
						    		<?PHP }else{?>
						    			<img class="w-100  shadow rounded" src="<?PHP echo base_url()?>assets/img/noimage.png"/>
						    		<?PHP }?>
						    	</div>
						    	<div class="col-md-6 mt-3">
						    		<?PHP if($getdocs->row()->img4 !=''){?>
						    			<img class="w-100  shadow rounded" src="<?PHP echo base_url()?>assets/pictures/<?PHP echo $getdocs->row()->img4;?>"/>
						    		<?PHP }else{?>
						    			<img class="w-100  shadow rounded" src="<?PHP echo base_url()?>assets/img/noimage.png"/>
						    		<?PHP }?>
						    	</div>
						    </div>
						</div>
					</div>
				</div>
				<?PHP }?>

				<!-- DATATABLE -->
				<?PHP if(!$selected_id){?>
				<div class="col-md-12 p-3 bg-white">
					<div class="row">
						<div class="col-md-8">
			                <div class="input-group mb-1" style="max-width: 400px">
			                  <span class="input-group-text" id="basic-addon1" style="background: transparent; color: #006634; border: solid #006634; border-width: 2px 0px 2px 2px;">
			                  	<i class="fas fa-calendar-alt"></i> 
			                  </span>
			                  <input class="form-control form-control-sm text-center" type="text" id="daterange" name="daterange" style="border: 2px solid #006634;" />
			                </div>
			                <small class="text-secondary"> *Pilih tanggal terlebih dahulu untuk melihat laporan.</small>
			            </div>
			            <div class="col-md-4">
			            	<button class="btn w-100 buttons-copy py-2 px-1" id="photogrid" style="font-weight:500;font-size:1.2rem;border: 3px solid #00322f;border-radius: 20px;"><i class="fas fa-photo-video"></i> Foto-Foto Dokumentasi Patroli</button>
			            </div>
			        </div>
			        <hr/>
					<div class="col-md-12">
						<table id="example" class="display <?PHP if($main_menu!='record'){ echo 'table-striped';}?>" style="width:100%">
			                <thead>
			                    <tr>
			                    	<?PHP if(!isset($is_no_hide)){?>
			                    		<th class="text-center align-middle">No.</th>
			                    	<?PHP }?>

			                        <?PHP foreach($datatable->list_fields() as $col){ 
			                        	if($is_id_hide){
			                        		if($col != $datatable_id){
			                        			echo "<th  class='text-center align-middle'>".$col."</th>";
			                        		}
			                        	}else{
			                        		if($col == 'NIK ACC' || $col == 'NIK Petugas'){
			                        			echo "<th class='text-center align-middle thenikdata'>".$col."</th>";
			                        		}else{
			                        			echo "<th class='text-center align-middle'>".$col."</th>";
			                        		}
			                        		
			                        	}
			                        	
			                        }?>
			                        <?PHP if($sub_menu == 'location'){?>
			                        	<th class="text-center align-middle">QR Code</th>
			                        <?PHP }?>
			                        <th class="text-center align-middle">Lihat/Ubah</th>
			                        <th class="text-center align-middle">Hapus</th>
			                    </tr>
			                </thead>
			                <tbody>
			                	<?PHP $i=0; foreach($datatable->result() as $row){ $theid=0; $i++;?>
			                	<tr>
			                		<?PHP if(!isset($is_no_hide)){?>
			                    		<td><?PHP echo $i; ?></td>
			                    	<?PHP }?>
			                		
			                		<?PHP foreach($row as $k => $v){ 
			                				if($k == 'Status ACC'){
			                					if($v == 'Accept'){
			                						echo "<td class='fw-bold text-success text-center align-middle'>Diterima</td>";
			                					}
			                					else if($v == 'Reject'){
			                						echo "<td class='fw-bold text-danger text-center align-middle'>Ditolak</td>";
			                					}else{
			                						echo "<td class='fw-bold text-warning text-center align-middle'>Menunggu ACC</td>";
			                					}
			                				}else if($k == 'Tanggal/waktu' || $k == 'Tanggal/waktu ACC'){
			                					if($v != '0000-00-00 00:00:00'){
		                							echo "<td class='text-center align-middle'>".date("d-m-Y H:i", strtotime($v)).'</td>';
		                						}else{
		                							echo '<td class="text-center align-middle"></td>';
		                						}
			                				}else if($is_id_hide){
				                        		if($k != $datatable_id){
				                        			echo "<td class='text-center align-middle'>".$v."</td>";
				                        		}else{
				                        			$theid = $v;
				                        		}
				                        	}else{
				                        		if($k == $datatable_id){
				                        			$theid = $v;
				                        		}
				                        		echo "<td class='text-center align-middle'>".$v."</td>";
				                        	}
			                		}?>
			                		<?PHP if($sub_menu == 'location'){?>
				                		<td>
				                			<form method="post" action="<?PHP echo base_url()."data/".$controller_name?>/qrcode">
				                				<input type="hidden" name="id" value="<?PHP echo $this->encryption->encrypt($theid);?>">
				                				<button class="btn buttons-copy btn-sm"><i class="fas fa-qrcode"></i> QR Code</button>
				                			</form>
				                			
				                		</td>
			                		<?PHP }?>
			                		<td>
			                        	<form method="post" action="<?PHP echo base_url()."data/".$controller_name?>">
			                				<input type="hidden" name="id" value="<?PHP echo $this->encryption->encrypt($theid);?>">
			                				<button type="submit" class="btn buttons-excel btn-sm sc btn-edit-link text-white"><i class="fas fa-edit"></i> Lihat/Ubah</button>
			                			</form>
			                		</td>
	                				<td>
			                        	<button onClick="deleteData('<?PHP echo $this->encryption->encrypt($theid);?>',<?PHP echo $i;?>)" class="btn btn-sm btn-danger sc btn-delete text-white">
			                        			<i class="fas fa-trash-alt"></i> Hapus
			                        	</button>
			                        </td>
			                	</tr>
			                	<?PHP }?>
			                </tbody>
			                <tfoot>
			                    <tr>
			                    	<?PHP if(!isset($is_no_hide)){?>
			                    		<th class="text-center align-middle">No.</th>
			                    	<?PHP }?>

			                        <?PHP foreach($datatable->list_fields() as $col){ 
			                        	if($is_id_hide){
			                        		if($col != $datatable_id){
			                        			echo "<th  class='text-center align-middle'>".$col."</th>";
			                        		}
			                        	}else{
			                        		if($col == 'NIK ACC' || $col == 'NIK Petugas'){
			                        			echo "<th class='text-center align-middle thenikdata'>".$col."</th>";
			                        		}else{
			                        			echo "<th class='text-center align-middle'>".$col."</th>";
			                        		}
			                        		
			                        	}
			                        	
			                        }?>
			                        <?PHP if($sub_menu == 'location'){?>
			                        	<th class="text-center align-middle">QR Code</th>
			                        <?PHP }?>
			                        <th class="text-center align-middle">Lihat/Ubah</th>
			                        <th class="text-center align-middle">Hapus</th>
			                    </tr>
			                </tfoot>
			            </table>
					</div>
				</div>
				<?PHP }?>
                </div>
			</div>
		</div>
	</div>
   
      

	<!-- Modal -->
	<div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="modalDelete" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modalDelete">
	        	Konfirmasi 
	        	<?PHP if($sub_menu == 'rev_awp'){?>
		        	Menolak 
		        <?PHP }else if($main_menu == 'record'){?>
		        	Mengembalikan
		        <?PHP }else{?>
		        	Menghapus
		        <?PHP }?>
	        	Data
	    	</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	        Apakah anda yakin akan 
	        <?PHP if($sub_menu == 'rev_awp'){?>
	        	menolak 
	        <?PHP }else if($main_menu == 'record'){?>
		        	mengembalikan ke pengaturan realisasi untuk 
	        <?PHP }else{?>
	        	menghapus
	        <?PHP }?>
	        data <span class="fw-bold">No.</span> <span id="numdel" class="fw-bold">-</span> ?
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Tidak!</button>
	        <form method="post" action="<?PHP echo base_url()."data/".$controller_name."/delete"?>">
				<input id="delete-id" type="hidden" name="id" value="">
				<button type="submit" class="btn btn-sm btn-danger">
					<?PHP if($sub_menu == 'rev_awp'){?>
			        	Ya, Tolak! 
			        <?PHP }else{?>
			        	Ya, Hapus!
			        <?PHP }?>
				</button>
			</form>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="modalFinalize" tabindex="-1" aria-labelledby="modalFinalize" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modalDelete">Konfirmasi Finalisasi Data</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	        Apakah anda yakin data <span class="fw-bold">No.</span> <span id="numfin" class="fw-bold">-</span> sudah selesai ?
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Tidak</button>
	        <form method="post" action="<?PHP echo base_url()."data/".$controller_name."/finalize"?>">
				<input id="finalize-id" type="hidden" name="id" value="">
				<button type="submit" class="btn btn-sm btn-primary">Ya, Selesai</button>
			</form>
	      </div>
	    </div>
	  </div>
	</div>



    <div id="footer" class="container-fluid pt-3 mt-3 pt-1 pb-3" style=" background:#e8ffe8; font-size: 0.8rem; border-top: 2px solid black;">
	    <div class="row text-center">
	        <center><img class="bi" src="<?PHP echo base_url()?>assets/img/kmblogo.png?cache=<?PHP echo Date("ymdhis");?>" style="width: 150px;"></center>
	    </div>
	</div>
  </body>

    <!-- Optional JavaScript; choose one of the two! -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/id.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js" ></script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
   <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/7.2.1/d3.min.js" integrity="sha512-wkduu4oQG74ySorPiSRStC0Zl8rQfjr/Ty6dMvYTmjZw6RS5bferdx8TR7ynxeh79ySEp/benIFFisKofMjPbg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
   <script type="text/javascript" src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
   <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script> -->
   <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

   <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <script type="text/javascript">
        const BASE_URL = "<?PHP echo base_url()?>"; 

        function hideShowOpt(id,val,cascade,isedit=0) {
        	// console.log(id,cascade,val);
        	if(!isedit){
        		$("#fid-"+id).val([]);
        	}
        	// console.log(".cls-opt-"+id+"[attr-"+cascade+"="+val+"]");
        	$(".cls-opt-"+id).hide();
        	$(".cls-opt-"+id+"[attr-"+cascade+"="+val+"]").show();
        } 
        function deleteData(id,number){
        	var deleteModal = new bootstrap.Modal(document.getElementById('modalDelete'), {
			  keyboard: false
			})
			$("#numdel").html(number)
			$("#delete-id").val(id);
        	deleteModal.show();
        }   

        function finalizeData(id,number){
        	var finalizeModal = new bootstrap.Modal(document.getElementById('modalFinalize'), {
			  keyboard: false
			})
			$("#numfin").html(number);
			$("#finalize-id").val(id);
        	finalizeModal.show();
        }


        $(document).ready(function() {

        	kol = $("#fid-indikator_awp_id").attr("attr-ref_indikator_id");
        	<?PHP if(!$selected_id){?>
	            OUR_DATA_TABLE = $('#example').DataTable({
	              dom: 'Blfrtip',
              "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"] ],
	              buttons: [
	              	  'copy',
	              	  'csv',
	                  {
	                    extend: 'excel',
	                    title: "LAPORAN PATROLI || <?PHP echo $pt_name?>",
	                    exportOptions: {
	                    	columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
	                    },
	                    messageTop: `
	                            PT. Kujang Mitra Bersama
	                            \nRentang Waktu : <?PHP echo $start_date_report?> WIB s/d <?PHP echo $end_date_report?> WIB
	                            `,
	                    messageBottom: `\nLaporan ini diunduh dari sistem aplikasi gatur patroli milik PT. Kujang Mitra Bersama, pada tanggal <?PHP echo Date('d/m/Y')?>.`
	                  },
	                  {
	                    extend: 'pdfHtml5',
	                    orientation: 'landscape',
	                    pageSize: 'LEGAL',
	                    exportOptions: {
	                    	columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
	                    },
	                    title: "LAPORAN PATROLI || <?PHP echo $pt_name?>",
	                    messageTop: `
	                            PT. Kujang Mitra Bersama
	                            \nRentang Waktu : <?PHP echo $start_date_report?> WIB s/d <?PHP echo $end_date_report?> WIB
	                            `,
	                    messageBottom: `\nLaporan ini diunduh dari sistem aplikasi gatur patroli milik PT. Kujang Mitra Bersama, pada tanggal <?PHP echo Date('d/m/Y')?>.`
	                  }
	              ],
	              "scrollX": true,
	            });
            <?PHP } ?>

            <?PHP if($form != null){foreach ($form as $key) {
	          		foreach ($af as $k => $v) {
	          			if(!isset($key[$k])){
	          				$key[$k] = $v;
	          			}
	          	    }
	          	    if(isset($key['js_cascade_field'])){
	          	    	if(isset($key['js_cascade_other_val'])){
	          	    		echo '$("#fid-'.$key['js_cascade_field'].'").on("change",function(){ hideShowOpt("'.$key['db_field_name'].'",$("#fid-'.$key['js_cascade_field'].'").find(":selected").attr("attr-'.$key['js_cascade_other_val'].'"),"'.$key['js_cascade_col'].'"); });';
	          	    	}else{
	          	    		echo '$("#fid-'.$key['js_cascade_field'].'").on("change",function(){ hideShowOpt("'.$key['db_field_name'].'",$("#fid-'.$key['js_cascade_field'].'").val(),"'.$key['js_cascade_col'].'"); });';
	          	    	}
	          	    	
	          	    }
	          	  }}
	         ?>

	       	$(function() {

                // var start = moment().subtract(1, 'days');
                // var end = moment();
                var start = moment("<?PHP echo $start_date?>", "YYYY-MM-DD");
                var end = moment("<?PHP echo $end_date?>", "YYYY-MM-DD");



                $('#daterange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    "autoApply": true,
                    "locale": {
                        "format": 'dddd DD/MM/YYYY',
                        "applyLabel": "Atur tanggal",
                        "cancelLabel": "Batal",
                        "fromLabel": "Dari",
                        "toLabel": "Sampai",
                        "customRangeLabel": "Tanggal lainnya",
                        "daysOfWeek": [
                            "Min",
                            "Sen",
                            "Sel",
                            "Rab",
                            "Kam",
                            "Jum",
                            "Sab"
                        ],
                        "monthNames": [
                            "Jan",
                            "Feb",
                            "Mar",
                            "Apr",
                            "Mei",
                            "Jun",
                            "Jul",
                            "Agu",
                            "Sep",
                            "Okt",
                            "Nov",
                            "Des"
                        ],
                    },
                    ranges: {
                       'Hari ini': [moment(), moment()],
                       'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       '7 hari terakhir': [moment().subtract(6, 'days'), moment()],
                       '30 hari terakhir': [moment().subtract(29, 'days'), moment()],
                       'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
                       'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                });

                $('#daterange').on('apply.daterangepicker', function(ev, picker) {
                  let startDate = picker.startDate.format('YYYY-MM-DD');
                  let endDate = picker.endDate.format('YYYY-MM-DD');
	              window.location.replace("<?PHP echo base_url();?>data/record?start_date="+startDate+"&end_date="+endDate);
                });

            });
        
            $("#photogrid").on("click",function(){ 
             let start = $('#daterange').data('daterangepicker').startDate.format('YYYY-MM-DD');
             let end = $('#daterange').data('daterangepicker').endDate.format('YYYY-MM-DD');
             window.location.replace("<?PHP echo base_url();?>data/record/photogrid?start_date="+start+"&end_date="+end);
            })

            $("#select_perusahaan").on('change',function(){
	         	  $.post( "<?PHP echo base_url()?>data/<?PHP echo $sub_menu?>/change_pt", { 
		              pt_id: $("#select_perusahaan").val(),
		              sub_menu: "<?PHP echo $sub_menu?>",
		          })
		          .done(function( data ) {
		            $("#theloader").hide();
		            window.location.replace("<?PHP echo base_url()?>data/<?PHP echo $sub_menu?>");
		          });
		         	
	         });
	        
        });
        
    </script>
    <script type="text/javascript" src="<?PHP echo base_url()?>assets/js/data/data-management.js?version=<?PHP echo date("YmdHis")?>"></script>

  </body>
</html>




