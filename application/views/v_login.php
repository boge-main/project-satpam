<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <title>Kujang Mitra Bersama | Login</title>
    <style type="text/css">
      .gradient-custom {
        /* fallback for old browsers */
        background: #006634;

        /*background-color: #ba4716;
        background-image: linear-gradient(43deg, #ba4716 0%, #e0ce64 46%, #2b5b49 100%);*/

      }
      *:not(.fas,.far){
        font-family: 'Roboto', sans-serif !important;
        font-weight: 300;
      }
      h1,h2,h3,h4,h5,h6,.big-title,b{
        font-weight: 400 !important;
      }

    </style>
  </head>
  <body class="gradient-custom">
    <section class="vh-100">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col-12 col-md-6 col-lg-4 col-xl-4">
            <div class="card text-dark rounded-3 shadow" style="background: #fff">
              <div class="card-body p-3 text-center">

                <div class="mb-md-5 mt-md-4 pb-3">
                  <img class="bi me-2" src="<?PHP echo base_url()?>assets/img/kmblogo.png?cache=<?PHP echo Date("ymdhis");?>" height="60">
                  <hr/>
                  <?PHP if($error){?>
                    <div class="alert alert-danger" role="alert">
                      Opps! NIK atau Password anda salah, silahkan coba lagi!
                    </div>
                  <?PHP }?>

                  <p class="text-dark-50 mb-3">Masukkan NIK dan Password anda</p>
                  <form method="post" action="<?PHP echo base_url()?>login/login">
                    <div class="form-outline form-white mb-2">
                      <input type="text" id="nik" name="nik" class="form-control" placeholder="NIK" />
                    </div>

                    <div class="form-outline form-white mb-2">
                      <input type="password" id="password" name="password" class="form-control flash-password" placeholder="Password" />
                      <small class="form-text text-muted hide" id="flashing" style="cursor: pointer;">Tampilkan password <i class="fas fa-eye"></i></small>
                    </div>

                    <p class="small mb-3 pb-lg-2 text-dark-50">Hubungi admin jika anda lupa password!</p>

                    <button class="btn btn-success btn-lg px-5" type="submit">Login</button>
                  </form>

                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script type="text/javascript">
      $("#flashing").on('click',function(){
          if($(this).hasClass('hide')){
            $(".flash-password").attr("type",'text');
            $(this).removeClass('hide');
            $(this).html('Sembunyikan password <i class="fas fa-eye-slash"></i>');
          }else{
            $(".flash-password").attr("type",'password');
            $(this).addClass('hide');
            $(this).html('Tampilkan password <i class="fas fa-eye"></i>');
          }
        })
    </script>
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>