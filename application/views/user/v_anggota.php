<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Security Checking Application">
    <meta name="keywords" content="Kujang Mitra Bersama, Security, QR Check">
    <meta name="author" content="Kujang Mitra Bersama">
      <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?PHP echo base_url()?>assets/img/favico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo base_url()?>assets/img/favico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo base_url()?>assets/img/favico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo base_url()?>assets/img/favico/favicon-16x16.png">
    <link rel="manifest" href="<?PHP echo base_url()?>assets/img/favico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?PHP echo base_url()?>assets/img/favico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url()?>assets/css/user/style.css?ver=<?PHP echo date("YmdHis")?>">

    <title>Kujang Mitra Bersama</title>
  </head>
  <div id="theloader" class="loading text-center text-white">
    <table class="w-100">
      <tr>
        <td style="height: 100vh;" class="align-middle text-center">
          <div class="loader"></div>
          <br/>
          Loading..
        </td>
      </tr>
    </table>
  </div>
  <body>
  <div style="min-height: 90vh">
    <header class="p-0 text-white shadow" style="background: #006634">
      <div class="container-fluid p-0">
        <div class="d-flex flex-wrap align-items-center justify-content-center">
          <a href="#" class="text-center align-items-center mb-0 text-white text-decoration-none">
            <img class="bi" src="<?PHP  echo base_url()?>assets/img/header2.png" width="100%"> 
          </a>
        </div>
      </div>
    </header>
    <div class="container-fluid pt-2" id="personal" style="background: #e8ffe8">
        <div class="row">
          <div class="col-12">
            <div class="row m-0">
              <div class="col-5 pe-0 ps-0">
                  <table>
                    <tr>
                      <td class="fw-bold"><?PHP echo $this->session->userdata('un');?></td>
                    </tr>
                    <tr>
                      <td><?PHP echo $this->session->userdata('uk');?></td>
                    </tr>
                    <tr>
                      <td><?PHP echo $this->session->userdata('us');?></td>
                    </tr>
                  </table>
              </div>
              <div class="col-7 text-center">
                <p class="mb-2" style="color: #006b3b">Patroli pada area:</p>
                <h5 style="font-weight: 700!important; color: #116839"><?PHP echo $this->session->userdata("upn");?></h5>
              </div>
            </div>
          </div>
      </div>
      <div class="mt-3 pb-2">
          <?PHP if(null !== $this->session->flashdata('success')){?>
          <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong>Yey!</strong> <?PHP echo $this->session->flashdata('success');?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          <?PHP }else if(null !== $this->session->flashdata('danger')){?>
          <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong>Oops!</strong> <?PHP echo $this->session->flashdata('danger');?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          <?PHP }else if(null !== $this->session->flashdata('warning')){?>
          <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
            <strong>Warning!</strong> <?PHP echo $this->session->flashdata('warning');?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          <?PHP }else if(null !== $this->session->flashdata('info')){?>
          <div class="alert alert-primary alert-dismissible fade show text-center" role="alert">
            <strong>Info!</strong> <?PHP echo $this->session->flashdata('info');?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          <?PHP }?>
      </div>
    </div>
    
    <ul class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="scan-tab" data-bs-toggle="tab" data-bs-target="#scanqr-code" type="button" role="tab" aria-controls="scanqr-code" aria-selected="true" style="border-radius: 10px 0px 0px 10px;"><i class="fas fa-qrcode logobuttonmenu"></i> <span class="textbuttonmenu">Scan QR Code</span></button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="report-tab" data-bs-toggle="tab" data-bs-target="#laporan-patroli" type="button" role="tab" aria-controls="laporan-patroli" aria-selected="false"><i class="far fa-file-alt logobuttonmenu"></i> <span class="textbuttonmenu">Laporan Patroli</span></button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="config-tab" data-bs-toggle="tab" data-bs-target="#configure" type="button" role="tab" aria-controls="configure" aria-selected="false" style="border-radius: 0px 10px 10px 0px;"><i class="fas fa-cog logobuttonmenu"></i><span class="textbuttonmenu">Pengaturan</span></button>
      </li>
    </ul>

    <div class="tab-content" id="nav-tabContent">   
      <div class="tab-pane fade show active" id="scanqr-code" role="tabpanel" aria-labelledby="scanqr-code">
        <div class="container-fluid text-center pt-4">
            <div class="text-center" id="scanarea">
                <button class="btn buttonscan-class" onclick="openCam()" style="font-size: 100px; color: #006634">
                    <img src="<?PHP echo base_url()?>assets/img/camera.png" style="width: 150px">
                </button>
                <p class="mb-5 mt-3 buttonscan-class text-secondary">
                  Tekan <b>gambar kamera</b> di atas untuk melakukan selfie & scan pada lokasi yang akan diperiksa
                </p>
            </div>
        </div>
      </div>
      <div class="tab-pane fade" id="laporan-patroli" role="tabpanel" aria-labelledby="laporan-patroli">
        <div id="divreport" class="container-fluid pt-3 pb-3 text-dark">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                      <span class="input-group-text" id="basic-addon1" style="background: transparent;"><i class="fas fa-calendar-alt"></i></span>
                      <input readonly class="form-control form-control-sm text-center" type="text" id="daterange" name="daterange" style="background: transparent;"/>
                    </div>
                </div>
            </div>
        </div>
        <?PHP 
            $arr_hari=["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
            $arr_bulan=["", "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
        ?>
        <div class="container-fluid px-0">
          <div class="row mx-0">
              <table id="thetable" class="table table-bordered" style="font-size: 0.7rem; min-width: 100%;">
                  <thead>
                      <tr  style="background: #e8ffe8; color: #000;">
                          <th class="text-center align-middle fw-bold">Status ACC</th>
                          <th class="text-center align-middle fw-bold" style="width: 70px">Waktu Scan</th>
                          <th class="text-center align-middle fw-bold">Lokasi</th>
                          <th class="text-center align-middle fw-bold" style="width: 100px">Nama Petugas</th>
                          <th class="text-center align-middle fw-bold" style="width: 100px">Nama ACC</th>
                          <th class="text-center align-middle fw-bold" style="width: 70px">Waktu ACC</th>
                          <th class="text-center align-middle fw-bold">ID</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?PHP foreach($record->result() as $row){
                        $arrcheckimg = [$row->img1,$row->img2,$row->img3,$row->img4];
                      ?>
                        <tr>
                            <td class="text-center align-middle">
                              <?PHP if($row->acc_status == ""){
                                  if(strtotime($row->record_date_time) < strtotime('-3 days')) {
                                    echo '<span class="text-secondary fw-bold modalcheck" onClick="showDetail('.$row->id_record.')" attr-data="'.$row->id_record.'">Expired</span>';
                                  }
                                  else if (stripos(json_encode($arrcheckimg),'.png') !== false){
                                    echo '<span class="text-warning fw-bold modalcheck" onClick="showDetail('.$row->id_record.')" attr-data="'.$row->id_record.'"><i class="fas fa-question-circle"></i> Menunggu ACC</span>';
                                  }else{
                                    echo '<span class="text-warning fw-bold modalcheck">Menunggu</span>';
                                  }
                                }else if($row->acc_status == "Accept"){
                                  echo '<span class="text-success fw-bold modalcheck" onClick="showDetail('.$row->id_record.')" attr-data="'.$row->id_record.'"><i class="fas fa-check-circle"></i> Diterima</span>';
                                }else if($row->acc_status == "Reject"){
                                  if(strtotime($row->record_date_time) < strtotime('-3 days')) {
                                    echo '<span class="text-secondary fw-bold modalcheck" onClick="showDetail('.$row->id_record.')" attr-data="'.$row->id_record.'">Expired</span>';
                                  }else{
                                    echo '<span class="text-danger fw-bold modalcheck">Ditolak</span>';
                                  }
                                  
                                }
                              ?>
                              <br/>
                              <?PHP if($row->acc_status == ""){
                                  if(strtotime($row->record_date_time) < strtotime('-3 days')) {
                                    // do something if any
                                  }else if (stripos(json_encode($arrcheckimg),'.png') !== false){
                                    // do something if any
                                  }else{
                                    echo '<button class="btn btn-sm btn-warning modalcheck" onClick="showDetail('.$row->id_record.')" attr-data="'.$row->id_record.'">Upload foto</button>';
                                  }
                                }else if($row->acc_status == "Reject"){
                                  if(strtotime($row->record_date_time) < strtotime('-3 days')) {
                                    // do something if any
                                  }else{
                                    echo '<button class="btn btn-sm btn-danger modalcheck" onClick="showDetail('.$row->id_record.')" attr-data="'.$row->id_record.'">Detail</button>';
                                  }
                                  
                                }
                              ?>
                            </td>
                            <td class="text-center align-middle">
                                <?PHP $date=date_create($row->record_date_time);
                                      $hari = $arr_hari[date_format($date, "w")];
                                      $bulan = $arr_bulan[date_format($date, "n")];
                                      $tahun = date_format($date,"Y");
                                      $tgl = date_format($date,"d");
                                      echo $hari.',<br/> '.$tgl.' '.$bulan.' '.$tahun.'<br/> '.date_format($date,"H:i").' WIB';
                                ?>        
                            </td>
                            <td class="text-center align-middle"><?PHP echo $row->location_name?></td>
                            
                            <td class="text-center align-middle"><?PHP echo $row->u_name?><br/>(<?PHP echo $row->u_nik?>)<br/>(<?PHP echo $row->u_level?>)</td>
                            <td class="text-center align-middle"><?PHP if($row->acc_by != "") echo $row->acc_by.'<br/>('.$row->us_nik.')<br/>('.$row->us_level.')'; else echo "--";?></td>
                            <td class="text-center align-middle">
                              <?PHP if($row->acc_date_time == "0000-00-00 00:00:00"){ 
                                        echo "--";
                                      }
                                      else {
                                        $date=date_create($row->acc_date_time);
                                        $hari = $arr_hari[date_format($date, "w")];
                                        $bulan = $arr_bulan[date_format($date, "n")];
                                        $tahun = date_format($date,"Y");
                                        $tgl = date_format($date,"d");
                                        echo $hari.',<br/> '.$tgl.' '.$bulan.' '.$tahun.'<br/> '.date_format($date,"H:i").' WIB';
                                               
                                      }
                                ?> 
                            </td>
                            <td class="text-center align-middle"><?PHP echo $row->id_record?></td>
                        </tr>
                      <?PHP }?>
                  </tbody>
              </table>
          </div>
        </div>
      </div>
      <div class="tab-pane container-fluid fade" id="configure" role="tabpanel" aria-labelledby="configure">
        <div class="col mt-4 text-center">
          <button type="button" class="btn px-5 py-2 btn-sm" data-bs-toggle="modal" data-bs-target="#modalpassword" style="color: #fff; font-size: 1rem;background: #006634; border-radius: 15px; font-size: 1.2rem; font-weight: 600">
            <i class="fas fa-cog"></i>&nbsp;&nbsp;Password
          </button>
        </div>
        <div class="col mt-4 text-center">
          <a href="<?PHP echo base_url()?>login/logout">
            <button type="button" class="btn" style="font-size: 1.2rem; font-weight: 400; color: #da3844;border: solid #da3844; border-width: 4px 0px 4px 0px; border-radius: 0px"><i class="fas fa-sign-out-alt"></i> Keluar</button>
          </a>
        </div>
        <div class="col mt-5 text-center">
          <p class="mb-5 buttonscan-class text-secondary">Tekan tombol <b>Password</b> di atas guna mengubah password untuk masuk ke aplikasi gatur patroli. Sedangkan untuk logout atau keluar dari aplikasi, tekan tombol <b>keluar</b>.</p>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modalpassword" tabindex="-1" aria-labelledby="modalpassword" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Ubah Password</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
          <form method="post" action="<?PHP echo base_url();?>user/user_conf/change_password">
            <div class="modal-body">
              <div class="mb-3">
                <label class="form-label">Password lama (saat ini)</label>
                <input type="password" class="form-control form-control-sm" name="lastpass">
              </div>
              <hr/>
              <div class="mb-3">
                <label class="form-label">Password Baru</label>
                <input type="password" class="form-control form-control-sm" name="newpass1">
              </div>
              <div class="mb-3">
                <label class="form-label">Verifikasi Password Baru</label>
                <input type="password" class="form-control form-control-sm" name="newpass2">
              </div>
            </div>
            <div class="modal-footer">
              <div type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</div>
              <button type="submit" class="btn btn-success">Simpan perubahan</button>
            </div>
          </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalcheck" tabindex="-1" aria-labelledby="modalcheck" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="position: sticky;top: 0px;background: white;">
            <h5 class="modal-title" id="titledetail">Detail</h5>
            <button type="button" class="btn btn-outline-dark" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
          </div>
          <div class="col-md-12 px-2">
            <div class="col-12">
              <div class="row">
                <div class="col-6 text-secondary" id="leftcol">

                </div>
                <div class="col-6 text-secondary" id="rightcol">

                </div>
                <div class="col-12 mt-3" id="keteranganACC" style="font-size: 0.7rem">
                  <span class="fw-bold">Keterangan:</span><br/>
                  <span class="fw-bold text-danger" id="keteranganACCtext"></span>
                </div>
              </div>
            </div>
            <center><h5 class="text-white mt-3 mb-1 py-1" style="background: #006634;">Hasil Scan QR Code</h5></center>
            <center id="theimage"></center>
          </div>
          <div class="col-md-12 px-2">
            <center><h5 class="text-white mt-3 py-1" style="background: #006634;">Foto Dokumentasi</h5></center>
          <table class="table table-borderless">
            <tr>
              <td class="text-center">
                <video id="video1"  class="dnone video" width="320" height="240" autoplay></video>
                <button id="snap1" class="snap dnone btn btn-sm btn-primary">Foto</button>
                <p id="template1" class="atemplate template"> <i class="fas fa-camera" style="font-size: 2rem;"></i> <br/> Tekan tombol <b>Ambil Gambar</b> dibawah, untuk mengaktifkan kamera!</p>
                <img id="imgsaved1" class="imgsaved dnone" src="#" width="320" />
                <canvas id="canvas1" width="320" height="240" class="border dnone canvas"><text></text></canvas>
                <br class="take-additional"/>
                <button id="take1" class="btn btn-success btn-sm take" attr-id="1"><i class="fas fa-camera"></i> Ambil Gambar </button>
              </td>
            </tr>
            <tr>
              <td class="text-center">
                <video id="video2"  class="dnone video" width="320" height="240" autoplay></video>
                <button id="snap2" class="snap dnone btn btn-sm btn-primary">Foto</button>
                <p id="template2" class="atemplate template"> <i class="fas fa-camera" style="font-size: 2rem;"></i> <br/> Tekan tombol <b>Ambil Gambar</b> dibawah, untuk mengaktifkan kamera!</p>
                <img id="imgsaved2" class="imgsaved dnone" src="#" width="320" />
                <canvas id="canvas2" width="320" height="240" class="border dnone canvas"><text></text></canvas>
                <br class="take-additional"/>
                <button id="take2" class="btn btn-success btn-sm take" attr-id="2"><i class="fas fa-camera"></i> Ambil Gambar </button>
              </td>
            </tr>
            <tr>
              <td class="text-center">
                <video id="video3"  class="dnone video" width="320" height="240" autoplay></video>
                <button id="snap3" class="snap dnone btn btn-sm btn-primary">Foto</button>
                <p id="template3" class="atemplate template"> <i class="fas fa-camera" style="font-size: 3rem;"></i> <br/> Tekan tombol <b>Ambil Gambar</b> dibawah, untuk mengaktifkan kamera!</p>
                <img id="imgsaved3" class="imgsaved dnone" src="#" width="320" />
                <canvas id="canvas3" width="320" height="240" class="border dnone canvas"><text></text></canvas>
                <br class="take-additional"/>
                <button id="take3" class="btn btn-success btn-sm take" attr-id="3"><i class="fas fa-camera"></i> Ambil Gambar </button>
              </td>
            </tr>
            <tr>
              <td class="text-center">
                <video id="video4"  class="dnone video" width="320" height="240" autoplay></video>
                <button id="snap4" class="snap dnone btn btn-sm btn-primary">Foto</button>
                <p id="template4" class="atemplate template"> <i class="fas fa-camera" style="font-size: 4rem;"></i> <br/> Tekan tombol <b>Ambil Gambar</b> dibawah, untuk mengaktifkan kamera!</p>
                <img id="imgsaved4" class="imgsaved dnone" src="#" width="320" />
                <canvas id="canvas4" width="320" height="240" class="border dnone canvas"><text></text></canvas>
                <br class="take-additional"/>
                <button id="take4" class="btn btn-success btn-sm take" attr-id="4"><i class="fas fa-camera"></i> Ambil Gambar </button>
              </td>
            </tr>
          </table>
        </div>
        <div class="col-md-12 text-center px-2 mb-2 take-additional">
          <center style="font-size: 0.8rem;">Jika foto-foto dokumentasi telah anda tambah/ubah, klik tombol <b>Simpan foto dokumentasi</b> untuk menyimpan foto-toto tersebut. jika anda batal menambahkan/mengubah, tekan tombol <b>Batal</b>.<br/>
          </center>
        </div>
        <div class="modal-footer take-additional">
          <center><div type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal/Tutup</div>
          <button  id="simpangambar" class="btn btn-primary">Unggah foto-foto</button></center>
        </div>
      </div>
    </div>
  </div>

  <div id="footer" class="container-fluid pt-3 mt-3 pt-1 pb-3" style=" background:#e8ffe8; font-size: 0.8rem; border-top: 2px solid black;">
      <div class="row text-center">
          <center><img class="bi" src="<?PHP echo base_url()?>assets/img/kmblogo.png?cache=<?PHP echo Date("ymdhis");?>" style="width: 150px;"></center>
      </div>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/id.min.js" ></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js" ></script>
  
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  <script type="text/javascript" src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript">
      let id_record = 0 
      let curfot = 0;
      let gambarambil = [0,0,0,0,0];
      let scanner= 0;
      let thestream = 0;

      $(document).ready(function(){
        $(".take").on('click',function(){
          if(thestream != 0){
            stopStream(thestream);
          };
          curfot = $(this).attr("attr-id");
          $("#video"+curfot).show();
          $("#snap"+curfot).show();
          $("#canvas"+curfot).hide();
          $("#take"+curfot).hide();
          $("#template"+curfot).hide();
          $("#imgsaved"+curfot).hide();

          var video = document.getElementById('video'+curfot);

          if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            const constraints = {
              video: { 
                facingMode: 'environment', 
                width: { min: 320, ideal: 320, max: 320 }, 
                height: { min: 240, ideal: 240, max: 240 } 
              }
            };

              navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
                  thestream = stream;
                  video.srcObject = stream;
                  video.play();
                  document.getElementById('snap'+curfot).addEventListener('click', function () {
                      takepicture();
                      stopStream(stream);
                  });
              }).catch( console.error );
          }
        })
        $("#simpangambar").on('click',function(){
            saveimage();
        })
      })
      

      function stopStream(stream) {
        stream.getVideoTracks().forEach(function (track) {
            track.stop();
        });
      }
      function takepicture() {
        $("#canvas"+curfot).show();
        var video = document.getElementById('video'+curfot);
        var canvas = document.getElementById('canvas'+curfot);

        canvas.height = video.videoHeight;
        canvas.width = video.videoWidth;

        var context = canvas.getContext('2d');
        context.drawImage(video, 0, 0, canvas.width, canvas.height);

        img = canvas.toDataURL();
        gambarambil[curfot] = img;

        $("#video"+curfot).hide();
        $("#snap"+curfot).hide();
        $("#canvas"+curfot).show();
        $("#take"+curfot).show();
        $("#template"+curfot).hide();
        $("#imgsaved"+curfot).hide();
       };
       function saveimage(){
          $("theloader").show();
          $.post( "<?PHP echo base_url()?>/scanqr/saveimage", { 
            id_record: id_record,
            img_nol: 0,
            img_satu: gambarambil[1],
            img_dua: gambarambil[2],
            img_tiga: gambarambil[3],
            img_empat: gambarambil[4],
          })
        .done(function( data ) {
          $("#theloader").hide();
          window.location.replace("<?PHP echo base_url()?>user/<?PHP echo $control?>?start_date=<?PHP echo explode(' ',$start_date)[0];?>&end_date=<?PHP echo explode(' ',$end_date)[0]?>&isrec=1");
        });
       }

       function resetpictures(){
        $(".video").hide();
        $(".snap").hide();
        $(".canvas").hide();
        $(".take").show();
        $(".template").show();
        $(".imgsaved").attr("src",'#');
        $(".imgsaved").hide();
       }



      function showDetail(val){
          
          if(scanner != 0){
            if (window.confirm('Batal Scan QR Code, reload laman web!'))
            {
                location.reload();
            }
            else
            {
                location.reload();
            }
          }

          $("#theloader").show();
        // $(".modalcheck").on('click',function(){
          gambarambil = [0,0,0,0,0];
          resetpictures();

          // let val = $(this).attr('attr-data');
          $("#detailInfo").html("");
          $.getJSON( "<?PHP echo base_url().'user/'.$control?>/getrecord?id="+val, function( data ) {
              $("#titledetail").html('');
              $("#leftcol").html('');
              $("#rightcol").html('');
              $(".take").show();
              $(".take-additional").show();
              $(".atemplate").show();
              $("#keteranganACC").hide();

              if(data[0][0].ACC == ''){
                if(moment(data[0][0].Waktu).isBefore(moment().subtract(3, "days"))){
                  $(".take").hide();
                  $(".take-additional").hide();
                  $(".atemplate").hide();
                  $("#keteranganACC").show();
                  $("#keteranganACCtext").html("Tidak bisa melakukan ACC, laporan diluar dari tanggal scan patroli");
                  $("#titledetail").html("<span class='text-secondary fw-bold'><i class='far fa-question-circle'></i> Expired ACC</span>");
                }else{
                  $("#titledetail").html("<span class='text-warning fw-bold'><i class='far fa-question-circle'></i> Menunggu ACC</span>");
                }
              }else if(data[0][0].ACC == 'Accept'){
                $(".take").hide();
                $(".take-additional").hide();
                $(".atemplate").hide();
                $("#titledetail").html("<span class='text-success  fw-bold'><i class='fas fa-check-circle'></i> Diterima</span>");
              }else if(data[0][0].ACC == 'Reject'){
                if(moment(data[0][0].Waktu).isBefore(moment().subtract(3, "days"))){
                  $(".take").hide();
                  $(".take-additional").hide();
                  $(".atemplate").hide();
                  $("#keteranganACC").show();
                  $("#keteranganACCtext").html("Tidak bisa melakukan ACC kembali, karena foto patroli tidak diganti oleh anggota yang tugas");
                  $("#titledetail").html("<span class='text-secondary fw-bold'><i class='fas fa-times-circle'></i> Expired ACC Tolak</span>");
                }else{
                  $("#titledetail").html("<span class='text-danger fw-bold'><i class='fas fa-times-circle'></i> Ditolak</span>");
                }
              }
              
              $("#leftcol").append("Waktu<br/><span class='fw-bold text-dark'>"+moment(data[0][0].Waktu).format('dddd, DD MMM YYYY, H:mm')+" WIB</span><br><br>");
              $("#leftcol").append("Titik Patroli<br/><span class='fw-bold text-dark'>"+data[0][0].Lokasi+"</span>");

              $("#rightcol").append("Nama Petugas<br/><span class='fw-bold text-dark'>"+data[0][0].Nama+"</span><br>");
              $("#rightcol").append("NIK<br/><span class='fw-bold text-dark'>"+data[0][0].NIK+"</span><br>");
              $("#rightcol").append("Jabatan<br/><span class='fw-bold text-dark'>"+data[0][0].level+"</span>");

              $("#theimage").html("<img src='"+data[0][0].image+"' style='width:100%'/>");
            id_record = val;

            $.each( data[1][0], function( key, val ) {
              key = key.replace("img",'');
              let randCache = (Math.random() + 1).toString(36).substring(7);
              if(val != ''){
                $("#template"+key).hide();
                $("#imgsaved"+key).show();
                $("#imgsaved"+key).attr("src","<?PHP echo base_url()?>assets/pictures/"+val+'?ch='+randCache);
              }
            });


            $("#modalcheck").modal("show");
            var myModalEl = document.getElementById('modalcheck')
            myModalEl.addEventListener('hidden.bs.modal', function (event) {
              if(thestream != 0){
                stopStream(thestream);
              }
            })
            $("#theloader").hide();
          });
        }
          
      $(document).ready(function(){
          
          $(".nav-link").on('click',function(){
            let id= $(this).attr("id");
            if(id=="report-tab"){
              setTimeout(function(){
                $('#thetable').dataTable().fnAdjustColumnSizing();
              },800);
            }
            parent.location.hash = id;
          });
          <?PHP if($isrec == 1){?>
            
            if(parent.location.hash){
              $(".nav-link").attr('class','nav-link');
              $(".tab-pane").attr('class','tab-pane fade');
              if(parent.location.hash == '#report-tab'){
                $("#report-tab").attr("class",'nav-link active');
                $("#laporan-patroli").attr('class','tab-pane fade show active');
              }else if(parent.location.hash == '#config-tab'){
                $("#config-tab").attr("class",'nav-link active');
                $("#configure").attr('class','tab-pane fade show active');
              }else{
                $("#scan-tab").attr("class",'nav-link active');
                $("#scanqr-code").attr('class','tab-pane fade show active');
              }
            }else{
              console.log('yo');
              $(".nav-link").attr('class','nav-link');
              $(".tab-pane").attr('class','tab-pane fade');
              $("#report-tab").attr("class",'nav-link active');
              $("#laporan-patroli").attr('class','tab-pane fade show active');
            }
            
          <?PHP }else{?>
            if(parent.location.hash){
              $(".nav-link").attr('class','nav-link');
              $(".tab-pane").attr('class','tab-pane fade');
              if(parent.location.hash == '#report-tab'){
                $("#report-tab").attr("class",'nav-link active');
                $("#laporan-patroli").attr('class','tab-pane fade show active');
              }else if(parent.location.hash == '#config-tab'){
                $("#config-tab").attr("class",'nav-link active');
                $("#configure").attr('class','tab-pane fade show active');
              }else{
                $("#scan-tab").attr("class",'nav-link active');
                $("#scanqr-code").attr('class','tab-pane fade show active');
              }
            }
          <?PHP }?>


          OUR_DATA_TABLE = $('#thetable').DataTable({
            // dom: 'lrtip',
            "dom": '<"top">rt<"bottom"lip><"clear">',
            "pagingType": "simple",
            "ordering": false,
            "lengthMenu": [ [5, 10, 25, 50, 100, -1], [5, 25, 10, 50, 100, "Semua"] ],
            // buttons: [
            //     {
            //       extend: 'copy',
            //       text: 'Salin',
            //       orientation: 'landscape',
            //       pageSize: 'LEGAL'
            //     },
            //     {
            //       extend: 'pdfHtml5',
            //       text: 'Download PDF',
            //       orientation: 'landscape',
            //       pageSize: 'LEGAL'
            //     }
            // ],
            "scrollX": true,
            "language": {
              "lengthMenu": "Menampilkan _MENU_ data per halaman",
              "zeroRecords": "Tidak ada data - Maaf",
              "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
              "infoEmpty": "Tidak ada data",
              "infoFiltered": "(Penyaringan dari _MAX_ total data)",
              "paginate": {
                  "first":      "Awal",
                  "last":       "Akhir",
                  "next":       "Selanjutnya",
                  "previous":   "Sebelumnya"
              },
              "search":         "",
              "searchPlaceholder": "Cari Kata kunci",
              // buttons: {
              //     copyTitle: 'Data telah berhasil disalin, silahkan paste/tempelkan data pada whatsapp, email, sms, dll.',
              //     copyKeys: 'Salin',
              //     copySuccess: {
              //         _: '%d Baris data telah disalin',
              //         1: '1 Baris data telah disalin'
              //     }
              // }
          }
          });
         

          $(function() {

              <?PHP if($isrec != 1){?>
              var start = moment().subtract(1, 'days');
              var end = moment();
              <?PHP }else{?>
                var start = moment('<?PHP echo $start_date?>');
                var end = moment('<?PHP echo $end_date?>');
              <?PHP }?>


              $('#daterange').daterangepicker({
                  startDate: start,
                  endDate: end,
                  "autoApply": true,
                  "locale": {
                      "format": 'dddd DD/MM/YYYY',
                      "applyLabel": "Atur tanggal",
                      "cancelLabel": "Batal",
                      "fromLabel": "Dari",
                      "toLabel": "Sampai",
                      "customRangeLabel": "Tanggal lainnya",
                      "daysOfWeek": [
                          "Min",
                          "Sen",
                          "Sel",
                          "Rab",
                          "Kam",
                          "Jum",
                          "Sab"
                      ],
                      "monthNames": [
                          "Januari",
                          "Februari",
                          "Maret",
                          "April",
                          "Mei",
                          "Juni",
                          "Juli",
                          "Agustus",
                          "September",
                          "Oktober",
                          "November",
                          "Desember"
                      ],
                  },
                  // ranges: {
                  //    'Hari ini': [moment(), moment()],
                  //    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  //    '7 hari terakhir': [moment().subtract(6, 'days'), moment()],
                  //    '30 hari terakhir': [moment().subtract(29, 'days'), moment()],
                  //    'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
                  //    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                  // },
              });

              $('.drp-calendar.right').hide();
              $('.drp-calendar.left').addClass('single');

              $('.calendar-table').on('DOMSubtreeModified', function() {
                var el = $(".prev.available").parent().children().last();
                if (el.hasClass('next available')) {
                  return;
                }
                el.addClass('next available');
                el.append('<span></span>');
              });

              $('#daterange').on('apply.daterangepicker', function(ev, picker) {
                let startDate = picker.startDate.format('YYYY-MM-DD');
                let endDate = picker.endDate.format('YYYY-MM-DD');
                window.location.replace("<?PHP echo base_url();?>user/anggota?start_date="+startDate+"&end_date="+endDate+"&isrec=1");
              });

          });
      })
      

      function reloadPage(){
        location.reload();
      }

      function openCam(){
          scanner = 0;
          $("#theloader").show();

          var jQueryScript = document.createElement('script');  
          jQueryScript.setAttribute('src','https://rawgit.com/schmich/instascan-builds/master/instascan.min.js');
          document.head.appendChild(jQueryScript);
          setTimeout(function(){
            $("#scanarea").html('<button onclick="reloadPage()" class="btn p-0 float-end text-secondary">Batal scan QR code <i class="fas fa-window-close text-dark"></i> </button><video class="mb-3 border rounded" id="preview" style="min-width:300px;max-width:100%;max-height:600px;"></video>');
            scanner = new Instascan.Scanner({ 
              video: document.getElementById('preview'),
              mirror: true,
              captureImage: true, 
            });

              scanner.addListener('scan', function (content,image) {
                $.post( "<?PHP echo base_url()?>scanqr", { value: content, scan: image })
                  .done(function( data ) {
                    $("#theloader").show();
                    // window.location.replace("<?PHP echo base_url()?>"+data);
                    window.location.replace("<?PHP echo base_url()?>user/<?PHP echo $control?>?start_date=<?PHP echo explode(' ',$start_date)[0];?>&end_date=<?PHP echo explode(' ',$end_date)[0]?>&isrec=1");
                  });
              });
              Instascan.Camera.getCameras().then(function (cameras) {
                if (cameras.length > 0) {
                  scanner.start(cameras[0]);
                  $("#theloader").hide();
                  setTimeout(function(){
                    $('html,body').animate({scrollTop: $("#scanarea").offset().top}, 100);
                  },1000);
                } else {
                  console.error('No cameras found.');
                }
              }).catch(function (e) {
                console.error(e);
              });
          },1000);  
      }

      
      

      
  </script>



  </body>
</html>