<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Security Checking Application">
    <meta name="keywords" content="Kujang Mitra Bersama, Security, QR Check">
    <meta name="author" content="Kujang Mitra Bersama">
      <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo base_url()?>assets/img/favico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?PHP echo base_url()?>assets/img/favico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo base_url()?>assets/img/favico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo base_url()?>assets/img/favico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo base_url()?>assets/img/favico/favicon-16x16.png">
    <link rel="manifest" href="<?PHP echo base_url()?>assets/img/favico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?PHP echo base_url()?>assets/img/favico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url()?>assets/css/user/style.css?ver=<?PHP echo date("YmdHis")?>">

    <title>Kujang Mitra Bersama | <?PHP echo $start_date.' s/d '.$end_date?></title>
    <?PHP if($control == 'record'){?>
      <style type="text/css">
        div.dt-buttons{
          width: 100%;
          text-align: center;
        }
      </style>
    <?PHP }?>
  </head>
  <body>
    <header class="p-0 text-white shadow" style="background: #006634">
      <div class="container-fluid p-0">
        <div class="d-flex flex-wrap align-items-center justify-content-center">
          <a href="#" class="text-center align-items-center mb-0 text-white text-decoration-none">
            <img class="bi" src="<?PHP  echo base_url()?>assets/img/header2.png" width="100%"> 
          </a>
        </div>
      </div>
    </header>
  	<div class="container-fluid mt-3">
  		<center>
        <?PHP if($control != 'record'){?>
          <a href="<?PHP echo base_url().'user/'.$control?>?start_date=<?PHP echo explode(' ',$start_date)[0]?>&end_date=<?PHP echo explode(' ',$end_date)[0]?>&isrec=1">
        <?PHP }else{?>
          <a href="<?PHP echo base_url().'data/record'?>?start_date=<?PHP echo explode(' ',$start_date)[0]?>&end_date=<?PHP echo explode(' ',$end_date)[0]?>&pt=<?PHP echo $pt?>">
        <?PHP }?>
          <button class="btn btn-sm text-white" style="background: #006c3b"><i class="fas fa-arrow-left"></i> Kembali ke laman utama</button>
        </a>
      </center>
  		<hr/>
      <div class="col-md-12 text-center" style="font-size: 0.8rem">
        <p class="mb-0">Foto Dokumentasi di area:</p>
        <h5 style="font-weight: 700!important;"><?PHP echo $this->session->userdata("upn");?></h5>
        <p><?PHP echo $start_date_report?> WIB  s/d  <?PHP echo $end_date_report?> WIB</p>
      </div>
  		<table id="thetable" class="table w-100" style="font-size: 0.7rem">
  			<thead>
  				<tr>
  					<th>Lokasi</th>
  					<th>Gambar 1</th>
  					<th>Gambar 2</th>
  					<th>Gambar 3</th>
  					<th>Gambar 4</th>
  				</tr>
  			</thead>
  			<tbody>
          <?PHP 
            $path0 = './assets/img/noimage.png';
            $type0 = pathinfo($path0, PATHINFO_EXTENSION);
            $data0 = file_get_contents($path0);
            $base640 = 'data:image/' . $type0 . ';base64,' . base64_encode($data0);
          ?>
  				<?PHP foreach($record->result() as $row){
  					$path1 = './assets/pictures/'.$row->img1;
  					$path2 = './assets/pictures/'.$row->img2;
  					$path3 = './assets/pictures/'.$row->img3;
  					$path4 = './assets/pictures/'.$row->img4;

            if(file_exists($path1) && strlen($row->img1) > 3){
              $type1 = pathinfo($path1, PATHINFO_EXTENSION);
              $data1 = file_get_contents($path1);
              $base641 = 'data:image/' . $type1 . ';base64,' . base64_encode($data1);
            }else{
              $base641 = "no-image";
            }
  					
  					if(file_exists($path2) && strlen($row->img2) > 3){
              $type2 = pathinfo($path2, PATHINFO_EXTENSION);
              $data2 = file_get_contents($path2);
              $base642 = 'data:image/' . $type2 . ';base64,' . base64_encode($data2);
            }else{
              $base642 = "no-image";
            }

            if(file_exists($path3) && strlen($row->img3) > 3){
    					$type3 = pathinfo($path3, PATHINFO_EXTENSION);
              $data3 = file_get_contents($path3);
              $base643 = 'data:image/' . $type3 . ';base64,' . base64_encode($data3);
            }else{
              $base643 = "no-image";
            }

            if(file_exists($path4) && strlen($row->img4) > 3){
    					$type4 = pathinfo($path4, PATHINFO_EXTENSION);
              $data4 = file_get_contents($path4);
              $base644 = 'data:image/' . $type4 . ';base64,' . base64_encode($data4);
            }else{
              $base644 = "no-image";
            }

  				?>
  				<tr>
            <td style="width:200px">
  					 <?PHP echo '<b>'.$row->location_name.'</b> <br/>'.$row->record_date_time;?>
             <br/><br/>
             <?PHP echo $row->u_name.' <br/>('.$row->u_nik.') <br/>('.$row->u_level.') <br/>[rec '.$row->id_record.']'?>     
            </td>
  					<td>
              <?PHP if(strlen($base641) > 40){?>
                <img class="img-fluid" class="base641" src="<?PHP echo $base641?>" width="320" height="240">
              <?PHP }else{?>
                <img class="img-fluid" class="base640" src="<?PHP echo $base640?>" style="max-width:320; max-height:240">
              <?PHP }?>
            </td>
  					<td>
              <?PHP if(strlen($base642) > 40){?>
                <img class="img-fluid" class="base642" src="<?PHP echo $base642?>" width="320" height="240">
              <?PHP }else{?>
                <img class="img-fluid" class="base640" src="<?PHP echo $base640?>" style="max-width:320; max-height:240">
              <?PHP }?>

            </td>
  					<td>
              <?PHP if(strlen($base643) > 40){?>
                <img class="img-fluid" class="base643" src="<?PHP echo $base643?>" width="320" height="240">
              <?PHP }else{?>
                <img class="img-fluid" class="base640" src="<?PHP echo $base640?>" style="max-width:320; max-height:240">
              <?PHP }?>
            </td>
  					<td>
              <?PHP if(strlen($base644) > 40){?>
                <img class="img-fluid" class="base644" src="<?PHP echo $base644?>" width="320" height="240">
              <?PHP }else{?>
                <img class="img-fluid" class="base640" src="<?PHP echo $base640?>" style="max-width:320; max-height:240">
              <?PHP }?>
            </td>
  				</tr>
  				<?PHP }?>
  			</tbody>
  		</table>
  	</div>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script type="text/javascript" src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

    <script type="text/javascript">
            
        $(document).ready(function(){
            OUR_DATA_TABLE = $('#thetable').DataTable({
              dom: 'Bt',
              "lengthMenu": [ [-1], ["Semua"] ],
              buttons: [
                  {
                    extend: 'pdfHtml5',
                    orientation: 'portrait',
                    pageSize: 'LEGAL',
                    text: '<i class="fas fa-file-pdf"></i> Download Dokumentasi Patroli',
                    title: "DOKUMENTASI PATROLI  ||  <?PHP echo $this->session->userdata('upn')?>",
                    messageBottom: `\n\nPT. Kujang Mitra Bersama \n Rentang Waktu : <?PHP echo $start_date_report?> WIB s/d <?PHP echo $end_date_report?> WIB\nLaporan ini diunduh dari sistem aplikasi gatur patroli milik PT. Kujang Mitra Bersama, pada tanggal <?PHP echo Date('d/m/Y')?>.`,
                    customize: function(doc) {
				                // console.log(doc);
            						var arr2 = $('.img-fluid').map(function(){
            		                      return this.src;
            		                }).get();
            						 
            						var row = 0;
            						var col = 1;
            						for (var i = 0; i < arr2.length; i++ ) {
            							if(i==0 || i%4 == 0 ){
            								row =row+1;
            								col = 1;
            								doc.content[1].table.body[row][col] = {
            		                         image: arr2[i],
            		                         width: 100
            		                       }
            		                       col+=1;
            							}else{
            								doc.content[1].table.body[row][col] = {
            		                         image: arr2[i],
            		                         width: 100
            		                        }
            		                        col+=1;
            							}
            	                       
            					    }
            					}
                  }
              ],
              "scrollX": true,
              "language": {
                "lengthMenu": "Menampilkan _MENU_ date per halaman",
                "zeroRecords": "Tidak ada data - Maaf",
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tidak ada data",
                "infoFiltered": "(Penyaringan dari _MAX_ total data)",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "Selanjutnya",
                    "previous":   "Sebelumnya"
                },
                "search":         "",
                "searchPlaceholder": "Cari Kata kunci",
                buttons: {
                    copyTitle: 'Data telah berhasil disalin, silahkan paste/tempelkan data pada whatsapp, email, sms, dll.',
                    copyKeys: 'Salin',
                    copySuccess: {
                        _: '%d Baris data telah disalin',
                        1: '1 Baris data telah disalin'
                    }
                }
               }
            });
        });
        

        
    </script>



  </body>
</html>