<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	
	public function index($err=0){
		$this->auth->goToPage();
		$data= array(
			'error' => 0,
		);
		if($err != 0){
			$data['error'] = 1;
		}
		$this->load->view('v_login.php',$data);
	}

	public function login(){
		$nik = $this->input->post('nik');
		$pass = $this->input->post('password');
		$cek = $this->db->query("SELECT * FROM user where u_nik = '$nik' ");
		if($cek->num_rows() == 1){
			if($this->encryption->decrypt($cek->row()->u_password) != $pass){
				redirect('login/index/1');
			}
			$idpt = $cek->row()->pt_id;
			$pt = $this->db->query("SELECT * FROM pt where id_pt = $idpt");
			if($pt->num_rows()){
				$ptname = $pt->row()->pt_name;
			}else{
				$ptname = "Perusahaan Tidak Terdaftar";
			}
			$stat = $cek->row()->u_level;
			$data = array(
				'uid'   		=> $this->encryption->encrypt($cek->row()->id_user),
				'ul'          	=> $this->encryption->encrypt($stat),
				'us'          	=> $stat,
				'up'          	=> $cek->row()->pt_id,
				'upn'			=> $ptname,
				'un'          	=> $cek->row()->u_name,
				'uk'          	=> $cek->row()->u_nik,
				'logged'		=> true
			);
			$this->session->set_userdata($data);

			if($stat == 'Admin'){
				redirect("data/record");

			}else if($stat == 'Manajemen' || $stat == 'Officer'){
				redirect("user/observer");

			}else if($stat == 'Chief'){
				redirect("user/chief");

			}else if($stat == 'Danru'){
				redirect("user/danru");
			
			}else if($stat == 'Anggota'){
				redirect("user/anggota");

			}else{
				$this->session->sess_destroy();
				redirect('login');
			}
		}else{
			$data= array(
				'error' => 1,
			);
			$this->load->view('v_login.php',$data);
		}
	}

	public function logout(){
		session_destroy();
		$this->session->sess_destroy();
		redirect("login");
	}

}
