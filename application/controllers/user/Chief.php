<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Chief extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->auth->isChief();
        date_default_timezone_set('Asia/Jakarta');
    }

    
    public function index(){
        $allowedpt = $this->session->userdata('up');
        
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');
        $isrec = $this->input->get('isrec');
        

        if($start_date == '' || $end_date == ''){
            $start_date = date('Y-m-d',strtotime("-1 days"));
            $end_date = date('Y-m-d');
        }
        $start_date = $start_date.' 00:00:00';
        $end_date = $end_date.' 23:59:59';

        $start_date_report = date("d-m-Y H:i", strtotime($start_date));
        $end_date_report = date("d-m-Y H:i", strtotime($end_date));

        
        $id = $this->encryption->decrypt($this->session->userdata("uid"));
        
        
        $record = $this->db->query("
                    SELECT r.*, r.record_date_time, u.u_name, u.u_level, u.u_nik, p.pt_name, l.location_name, us.u_name as acc_by, us.u_level as us_level, us.u_nik as us_nik, pc.*  
                    FROM (SELECT * FROM record WHERE record_date_time >= '$start_date' AND record_date_time <= '$end_date' AND pt_id = $allowedpt AND is_active = 1)as r
                    LEFT JOIN `user` as u ON r.user_id = u.id_user
                    LEFT JOIN `user` as us ON r.acc_by_id = us.id_user
                    LEFT JOIN `pt` as p ON r.pt_id = p.id_pt
                    LEFT JOIN `location` as l ON r.location_id = l.id_location
                    LEFT JOIN `pictures` as pc ON r.id_record = pc.record_id
                    ORDER BY r.id_record DESC
                ");


        $data = array(
            'record'    => $record,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'isrec'     => $isrec,
            'start_date_report' => $start_date_report,
            'end_date_report' => $end_date_report,
            'control'   => 'chief', 
        );
        $this->load->view('user/v_chief.php',$data);
    }


    public function getrecord(){
        $allowedpt = $this->session->userdata('up');
        $id = $this->input->get('id');

        $getdt = $this->db->query("SELECT * FROM record WHERE id_record = $id");
        if($getdt->num_rows()){
            if($getdt->row()->pt_id != $allowedpt){
                echo "it works! [98031]";
            die();
            }
        }else{
            echo "it works! [98032]";
            die();
        }

        $getdata = $this->db->query("SELECT r.record_date_time as Waktu, p.pt_name as Perusahaan, l.location_name as Lokasi, u.u_name as Nama, u.u_nik as NIK, u.u_level as level, r.acc_status as ACC, p.image
                        FROM (SELECT * FROM scanned_image WHERE record_id = $id) as p
                        LEFT JOIN record as r ON p.record_id = r.id_record
                        LEFT JOIN `user` as u ON r.user_id = u.id_user
                        LEFT JOIN `pt` as p ON r.pt_id = p.id_pt
                        LEFT JOIN `location` as l ON r.location_id = l.id_location");
        $getpictures=$this->db->query("SELECT * FROM pictures where record_id = $id");
        if($getdata->num_rows() == 1){
            print_r(json_encode([$getdata->result(),$getpictures->result()])); 
        }else{
            print_r("{error:true}");
        }
    }

    public function check(){
        $allowedpt = $this->session->userdata('up');
        $idrecord = $this->input->post('idrecord');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $getdt = $this->db->query("SELECT * FROM record WHERE id_record = $idrecord");
        if($getdt->num_rows()){
            if($getdt->row()->pt_id != $allowedpt){
                echo "it works! [98041]";
            die();
            }
        }else{
            echo "it works! [98042]";
            die();
        }

        $terima = $this->input->post('terima');
        $tolak = $this->input->post('tolak');

        if($terima == 1){
            $data=array(
                'acc_by_id'     => $this->encryption->decrypt($this->session->userdata("uid")),
                'acc_status'    => "Accept",
                'acc_date_time' => date("Y-m-d H:i:s"),
            );
            $this->simple->update('record',$data,'id_record',$idrecord);
        }
        else if($tolak == 1){
            $data=array(
                'acc_by_id'     => $this->encryption->decrypt($this->session->userdata("uid")),
                'acc_status'    => "Reject",
                'acc_date_time' => date("Y-m-d H:i:s"),
            );
            $this->simple->update('record',$data,'id_record',$idrecord);
        }
        $this->session->set_flashdata('success', 'Terimakasih, ACC telah dilakukan');

        $start_date = explode(" ", $start_date);
        $start_date = $start_date[0];
        $end_date = explode(" ", $end_date);
        $end_date = $end_date[0];

        redirect('user/chief?start_date='.$start_date.'&end_date='.$end_date.'&isrec=1');
    }


    public function photogrid(){
        $allowedpt = $this->session->userdata('up');
        
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        
        if($start_date == '' || $end_date == ''){
            $start_date = date('Y-m-d',strtotime("-1 days"));
            $end_date = date('Y-m-d');
        }
        $start_date = $start_date.' 00:00:00';
        $end_date = $end_date.' 23:59:59';

        $start_date_report = date("d-m-Y H:i", strtotime($start_date));
        $end_date_report = date("d-m-Y H:i", strtotime($end_date));

                
        $id = $this->encryption->decrypt($this->session->userdata("uid"));

        
        $record = $this->db->query("
            SELECT ps.*, p.pt_name, l.location_name, r.record_date_time, u.u_name, u.u_nik, u.u_level, r.id_record
            FROM pictures as ps
            LEFT JOIN `record` as r ON ps.record_id = r.id_record
            LEFT JOIN `user` as u ON r.user_id = u.id_user
            LEFT JOIN `pt` as p ON r.pt_id = p.id_pt
            LEFT JOIN `location` as l ON r.location_id = l.id_location
            WHERE r.record_date_time >= '$start_date' AND r.record_date_time <= '$end_date' AND r.pt_id = $allowedpt AND r.is_active = 1
            ORDER BY r.id_record DESC
        ");


        $data = array(
            'record'    => $record,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'start_date_report' => $start_date_report,
            'end_date_report' => $end_date_report,
            'control'   => 'chief', 
        );
        $this->load->view('user/v_photogrid.php',$data);
    }


}
