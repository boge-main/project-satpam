<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_conf extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->auth->isLogin();
	}

	
	public function index(){
		$this->load->view('user/v_anggota.php');
	}

	public function change_password(){
		$lastpass = $this->input->post('lastpass');
		$newpass1 = $this->input->post('newpass1');
		$newpass2 = $this->input->post('newpass2');
		if($newpass1 != $newpass2){
			$this->session->set_flashdata('danger', 'Password gagal diubah karena verifikasi password baru salah, silahkan coba lagi!');
			$this->auth->goToPage('#config-tab');
		}else{
			$id_user = $this->encryption->decrypt($this->session->userdata("uid"));
			$get = $this->db->query("SELECT * FROM user WHERE id_user = $id_user");
			if($get->num_rows() == 1){
				if($lastpass == $this->encryption->decrypt($get->row()->u_password)){
					$data=array(
						'u_password' => $this->encryption->encrypt($newpass2),
					);
					$this->simple->update('user',$data,'id_user',$id_user);

					$this->session->set_flashdata('success', 'Password berhasil diubah.');
					$this->auth->goToPage('#config-tab');
					
				}else{
					$this->session->set_flashdata('danger', 'Password gagal diubah karena password lama salah, silahkan coba lagi!');
					$this->auth->goToPage('#config-tab');
				}
			}else{
				$this->session->set_flashdata('danger', 'Error [145.1]. Silahkan hubungi admin!');
				$this->auth->goToPage('#config-tab');
			}
		}
	}

}
