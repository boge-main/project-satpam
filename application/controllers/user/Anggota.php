<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anggota extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->auth->isAnggota();
		date_default_timezone_set('Asia/Jakarta');
	}

	
	public function index(){
		$start_date = $this->input->get('start_date');
		$end_date = $this->input->get('end_date');
		$isrec = $this->input->get('isrec');
		
		if($start_date == '' || $end_date == ''){
			$start_date = date('Y-m-d',strtotime("-1 days"));
			$end_date = date('Y-m-d');
		}
		$start_date = $start_date.' 00:00:00';
		$end_date = $end_date.' 23:59:59';

		$id = $this->encryption->decrypt($this->session->userdata("uid"));
	
		$record = $this->db->query("
						SELECT r.*, r.record_date_time, u.u_name, u.u_nik, u.u_level, p.pt_name, l.location_name, us.u_name as acc_by, us.u_level as us_level, us.u_nik as us_nik, pc.* 
						FROM (SELECT * FROM record WHERE user_id = $id AND record_date_time >= '$start_date' AND record_date_time <= '$end_date' AND is_active = 1)as r
						LEFT JOIN `user` as u ON r.user_id = u.id_user
						LEFT JOIN `user` as us ON r.acc_by_id = us.id_user
						LEFT JOIN `pt` as p ON r.pt_id = p.id_pt
						LEFT JOIN `location` as l ON r.location_id = l.id_location
						LEFT JOIN `pictures` as pc ON r.id_record = pc.record_id
						ORDER BY r.id_record DESC
					");
		$data = array(
			'record'	=> $record,
			'control'	=> 'anggota', 
			'isrec'		=> $isrec,
			'start_date'	=> $start_date,
			'end_date'	=> $end_date,
		);
		$this->load->view('user/v_anggota.php',$data);
	}

	public function getrecord(){
    	$id = $this->input->get('id');
    	$getdata = $this->db->query("SELECT r.record_date_time as Waktu, p.pt_name as Perusahaan, l.location_name as Lokasi, u.u_name as Nama, u.u_nik as NIK, u.u_level as level, r.acc_status as ACC, p.image
						FROM (SELECT * FROM scanned_image WHERE record_id = $id) as p
						LEFT JOIN record as r ON p.record_id = r.id_record
						LEFT JOIN `user` as u ON r.user_id = u.id_user
						LEFT JOIN `pt` as p ON r.pt_id = p.id_pt
						LEFT JOIN `location` as l ON r.location_id = l.id_location 
						ORDER BY r.id_record DESC 
					");
    	$getpictures=$this->db->query("SELECT * FROM pictures where record_id = $id");
    	if($getdata->num_rows() == 1){
			print_r(json_encode([$getdata->result(),$getpictures->result()])); 
		}else{
			print_r("{error:true}");
		}
    }

}
