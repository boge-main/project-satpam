<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/* FORM INFORMATION 
	$field = [
		{
			"db_field_name" 		: "kabupaten", // i: untuk dijadikan field id, harus menggunakan nama field di db, pastikan harus unik
			"html_field_name"		: "Kabupaten",  // i: akan dijadikan nama kolom isian (nama field di HTML)
			"html_readonly"			: true, untuk matiin input
			"html_init_value"		: "3101", // i: untuk set default value di kolom isian
			"html_type_input" 		: "select", //value: text|number|select|textarea|date|email| = https://www.w3schools.com/tags/tag_input.asp
			"html_placeholder"		: "Please select an option", // i: placeholder (text before input)
			"html_field_note"		: "please ensure your province is correct", // i: untuk memberikan catatan kaki pada field form
			"html_max"				: null, // i: max value, berlaku untuk field date|number
			"html_min"				: null, // i: min value, berlaku untuk field date|number
			"html_required"			: true|false,
			"html_options"			: $this->db->query("SELECT * FROM ref_kabupaten ORDER BY rp_name")->result(), // i: CI query, data pada select option
			"html_option_value"		: "id_ref_provinsi", // i: value dari query yang akan dijadikan base value untuk kolom ini
			"html_option_text"		: ["id_ref_provinsi","rp_name"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)
			"html_option_multiple"	; true, //multiple selection
			"js_cascade_field"		: "ref_provinsi_id", // i: nama field lain yang akan mempengaruhi selection field ini
			"js_cascade_col"		: "ref_provinsi_id", // i: FK . nama attribut di field ini yang akan dipengaruhi oleh isian field lain
			"custom_id"				: "sel_lokasi", //i: id field, jika null, id_field akan menggunakan db_field_name
			"custom_class"			: "class_select", //i: jika ingin menambahkan kelas pada field ini
			"custom_js"				: "$('#sel_lokasi').on('change',function(console.log('done')))", //i: additional javascript
		},
	];
 */ 


//============================================ PLEASE CHANGE THIS SETTINGS ========================================
class Record extends CI_Controller {
//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

	function __construct()
	{
		parent::__construct();
		$this->auth->isAdmin();

		$this->FORM = $this->form();
		date_default_timezone_set('Asia/Jakarta');


		//============================================ PLEASE CHANGE THIS SETTINGS ========================================
		$this->main_table = "record";
		$this->primary_key = "id_record";
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================


		
	}

	private function form(){

		//============================================ PLEASE CHANGE THIS SETTINGS ========================================
		$field = array(
			[
				"db_field_name" 		=> "pt_id",
				"html_required"			=> true,
				"html_field_name"		=> "Nama Perusahaan",
				"html_type_input" 		=> "select",
				"html_options"			=> $this->db->query("SELECT * FROM pt WHERE is_active = 1 ORDER BY pt_name")->result(), // i: CI query, data pada select option
				"html_option_value"		=> "id_pt", // i: value dari query yang akan dijadikan base value untuk kolom ini
				"html_option_text"		=> ["pt_name"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)	
			],
			[
				"db_field_name" 		=> "location_id",
				"html_required"			=> true,
				"html_field_name"		=> "Nama Lokasi",
				"html_type_input" 		=> "select",
				"html_options"			=> $this->db->query("SELECT * FROM `location` WHERE is_active = 1  ORDER BY location_name")->result(), // i: CI query, data pada select option
				"html_option_value"		=> "id_location", // i: value dari query yang akan dijadikan base value untuk kolom ini
				"html_option_text"		=> ["location_name"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)
                "js_cascade_field"		=> "pt_id", // i: nama field lain yang akan mempengaruhi selection field ini
			    "js_cascade_col"		=> "pt_id", // i: FK . nama attribut di field ini yang akan dipengaruhi oleh isian field lain	
			],

			[
				"db_field_name" 		=> "user_id",
				"html_required"			=> true,
				"html_field_name"		=> "Nama Pengguna",
				"html_type_input" 		=> "select",
				"html_options"			=> $this->db->query("SELECT * FROM user WHERE is_active = 1  ORDER BY u_name")->result(), // i: CI query, data pada select option
				"html_option_value"		=> "id_user", // i: value dari query yang akan dijadikan base value untuk kolom ini
				"html_option_text"		=> ["u_name", "u_nik", "u_level"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)	
			],
			[
				"db_field_name" 		=> "acc_by_id",
				"html_required"			=> true,
				"html_field_name"		=> "ACC Oleh",
				"html_type_input" 		=> "select",
				"html_options"			=> $this->db->query("SELECT * FROM user WHERE is_active = 1 ORDER BY u_name")->result(), // i: CI query, data pada select option
				"html_option_value"		=> "id_user", // i: value dari query yang akan dijadikan base value untuk kolom ini
				"html_option_text"		=> ["u_name", "u_nik", "u_level"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)	
			],
			[
				"db_field_name" 		=> "record_date_time",
				"html_required"			=> true,
				"html_field_name"		=> "Waktu Record",
				"html_type_input" 		=> "text",		
			],
			[
				"db_field_name" 		=> "acc_date_time",
				"html_required"			=> true,
				"html_field_name"		=> "Waktu ACC",
				"html_type_input" 		=> "text",		
			],
			[
				"db_field_name" 		=> "acc_status",
				"html_required"			=> true,
				"html_field_name"		=> "Status ACC",
				"html_type_input" 		=> "select",
				"html_options"		    => array(array("status" => "Accept"), array("status" => "Reject")),	
				"html_option_multiple"	=> false,	
				"html_option_value"		=> "status",
				"html_option_text"		=> ['status'],			
			],
		);
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

		return $field;
	}




	public function index()
	{
		$start_date = $this->input->get('start_date');
		$end_date = $this->input->get('end_date');
		$pt = $this->session->userdata("up");
		
		if($start_date == '' || $end_date == ''){
			if($this->session->userdata("start_date") != null){
				$start_date = $this->session->userdata("start_date");
				$end_date = $this->session->userdata("end_date");
			}else{
				$start_date = date('Y-m-d',strtotime("-1 days"));
				$end_date = date('Y-m-d');
			}
		}else{
			$data_session = array(
				'start_date' => $start_date,
				'end_date' => $end_date,
			);
			$this->session->set_userdata($data_session);
		}


		$start_date = $start_date.' 00:00:00';
		$end_date = $end_date.' 23:59:59';


		$updateid= $this->encryption->decrypt($this->input->post('id'));
		$datatable = array();
        $getimage = null;
        $getdocs = null;
		if($updateid){
			$getval = (array) $this->db->query("SELECT * FROM $this->main_table WHERE $this->primary_key = $updateid")->result();
			$getval = (array) $getval[0];
			foreach ($this->FORM as $key => $value) {
				$this->FORM[$key]['html_init_value'] = $getval[$this->FORM[$key]['db_field_name']];
			}
            $getimage = $this->db->query("SELECT * FROM scanned_image WHERE record_id = $updateid");
            $getdocs = $this->db->query("SELECT * FROM pictures WHERE record_id = $updateid");
		}
		$HTMLDATA['form'] = $this->FORM;
		$HTMLDATA['selected_id'] = $updateid;
		$HTMLDATA['datatable'] = &$datatable;
        $HTMLDATA['getimage'] = $getimage;
        $HTMLDATA['getdocs'] = $getdocs;



		//============================================ PLEASE CHANGE THIS SETTINGS ========================================

		if(!$this->input->get('id')){
			$datatable = $this->db->query("SELECT   l.location_name as 'Titik Patroli', 
													r.record_date_time as 'Tanggal/waktu',
													user.u_name as 'Nama Petugas', 
													user.u_nik as 'NIK Petugas', 
													user.u_level as 'Level Petugas', 
													acc_status as 'Status ACC',
													r.acc_date_time as 'Tanggal/waktu ACC', 
													u.u_name as 'Nama ACC', 
													u.u_nik as 'NIK ACC', 
													u.u_level as 'Level ACC',
													r.id_record as ID
										FROM (SELECT * FROM record WHERE record_date_time >= '$start_date' AND record_date_time <= '$end_date' AND pt_id = $pt AND is_active = 1 ) as r 
                                        LEFT JOIN pt ON r.pt_id = pt.id_pt
                                        LEFT JOIN user ON r.user_id = user.id_user
                                        LEFT JOIN user as u ON r.acc_by_id = u.id_user
                                        LEFT JOIN `location` as l ON r.location_id = l.id_location
                                        ORDER BY r.id_record DESC");
		}

		$pt_name = $this->db->query("SELECT pt_name FROM pt WHERE id_pt = $pt");
		$pt_name = $pt_name->row()->pt_name;

		$aptname = [];

		$HTMLDATA['pt']  = $this->db->query("SELECT * FROM pt WHERE is_active = 1 ORDER BY pt_name DESC ");
    	$HTMLDATA['up']  = $pt;
    	$HTMLDATA['pt_name']  = $pt_name;
    	$HTMLDATA['start_date']  = $start_date;
    	$HTMLDATA['end_date']  = $end_date;
    	$HTMLDATA['start_date_report']  = date("d-m-Y H:i", strtotime($start_date));
    	$HTMLDATA['end_date_report']    = date("d-m-Y H:i", strtotime($end_date));
		// HTML INFORMATION
		$HTMLDATA["controller_name"] 	= "record"; // nama controller untuk URL
		$HTMLDATA["main_menu"] 			= "data"; // main menu yang dibuka/aktif
		$HTMLDATA["sub_menu"] 			= "record";	// sub menu yang aktif

		// DATATABLE SETTING
		$HTMLDATA["datatable_id"] 		= "ID"; // primary key data, pastikan sama dengan query diatas
		$HTMLDATA["is_id_hide"] 		= false; // kalau id tidak ingin ditampilkan di datatable, set True
		
		
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

		

		$this->load->view("data/v_data_record",$HTMLDATA);
	}








	
	public function delete(){
		$updateid= $this->encryption->decrypt($this->input->post('id'));
		if($updateid){
			$data =array(
				"is_active" => 0
			);
			$update = $this->simple->update($this->main_table,$data,$this->primary_key,$updateid);
			$this->session->set_flashdata('info', 'Data berhasil dihapus');
		}
		redirect('data/'.$this->main_table);
	}

	public function input(){
		$id = $this->encryption->decrypt($this->input->post('id'));
		$form = $this->form();
		$data = array();
		foreach ($form as $f) {
			$data[$f['db_field_name']] = $this->input->post($f['db_field_name']);
			$data['created_at']	= date("Y-m-d H:i:s");
			$data['is_active']	= 1;
		}
		//============ IF YOU NEED TO CHANGE THE VALUE MANUALLY =======
			/* IF YOU NEED TO CHANGE THE VALUE MANUALLY
				print_r($data); //kalau mau ngecek data yang bakal diinputin ke DB, 
				$data['rk_name'] = "ganti nama";  //kalau misalnya mau manual nimpah data
			*/
		//============ END - IF YOU NEED TO CHANGE THE VALUE MANUALLY =======
		
		if(!$id){ //INSERT NEW ROW	
			$insert = $this->simple->insert($this->main_table,$data);
			if(isset($insert['code'])){
				$this->session->set_flashdata('danger', 'Data gagal dimasukkan, '.$insert['message']);
			}else{
				$this->session->set_flashdata('success', 'Data baru berhasil dimasukkan');
			}
			redirect('data/'.$this->main_table);

		}else{ // UPDATE EXISTING ROW
			$update = $this->simple->update($this->main_table,$data,$this->primary_key,$id);
			if(isset($update['code'])){
				$this->session->set_flashdata('danger', 'Data gagal diubah, '.$update['message']);
			}else{
				$this->session->set_flashdata('success', 'Data berhasil diperbaharui');
			}
			redirect('data/'.$this->main_table);
		}
		
	}



	public function photogrid(){
    	
    	$start_date = $this->input->get('start_date');
		$end_date = $this->input->get('end_date');
		$allowedpt = $this->session->userdata('up');
		
		if($start_date == '' || $end_date == ''){
			$start_date = date('Y-m-d',strtotime("-1 days"));
			$end_date = date('Y-m-d');
		}
		$start_date = $start_date.' 00:00:00';
		$end_date = $end_date.' 23:59:59';

		$start_date_report = date("d-m-Y H:i", strtotime($start_date));
		$end_date_report = date("d-m-Y H:i", strtotime($end_date));

				
		$id = $this->encryption->decrypt($this->session->userdata("uid"));

		
		$record = $this->db->query("
			SELECT ps.*, p.pt_name, l.location_name, r.record_date_time, u.u_name, u.u_nik, u.u_level, r.id_record
			FROM pictures as ps
			LEFT JOIN `record` as r ON ps.record_id = r.id_record
			LEFT JOIN `user` as u ON r.user_id = u.id_user
			LEFT JOIN `pt` as p ON r.pt_id = p.id_pt
			LEFT JOIN `location` as l ON r.location_id = l.id_location
			WHERE r.record_date_time >= '$start_date' AND r.record_date_time <= '$end_date' AND r.pt_id = $allowedpt AND r.is_active = 1
			ORDER BY r.id_record DESC
		");


    	$data = array(
    		'record'			=> $record,
    		'start_date' 		=> $start_date,
    		'end_date' 			=> $end_date,
    		'start_date_report' => $start_date_report,
    		'end_date_report' 	=> $end_date_report,
    		'control'			=> 'record',
    		'pt'				=> $allowedpt, 
    	);
        $this->load->view('user/v_photogrid.php',$data);
    }

    public function change_pt(){
		$pt_id= $this->input->post('pt_id');
		$menu= $this->input->post('sub_menu');

		$pt_name = $this->db->query("SELECT pt_name FROM pt where id_pt = $pt_id");
        $pt_name = $pt_name->row()->pt_name;
        
		$data = array(
			'up' => $pt_id,
			'upn' => $pt_name
		);
		$this->session->set_userdata($data);

		$id_user = $this->encryption->decrypt($this->session->userdata("uid"));
		$data=array(
			'pt_id' => $pt_id,
		);
		$this->simple->update('user',$data,'id_user',$id_user);
	}

}
