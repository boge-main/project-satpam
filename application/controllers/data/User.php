<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/* FORM INFORMATION 
	$field = [
		{
			"db_field_name" 		: "kabupaten", // i: untuk dijadikan field id, harus menggunakan nama field di db, pastikan harus unik
			"html_field_name"		: "Kabupaten",  // i: akan dijadikan nama kolom isian (nama field di HTML)
			"html_readonly"			: true, untuk matiin input
			"html_init_value"		: "3101", // i: untuk set default value di kolom isian
			"html_type_input" 		: "select", //value: text|number|select|textarea|date|email| = https://www.w3schools.com/tags/tag_input.asp
			"html_placeholder"		: "Please select an option", // i: placeholder (text before input)
			"html_field_note"		: "please ensure your province is correct", // i: untuk memberikan catatan kaki pada field form
			"html_max"				: null, // i: max value, berlaku untuk field date|number
			"html_min"				: null, // i: min value, berlaku untuk field date|number
			"html_required"			: true|false,
			"html_options"			: $this->db->query("SELECT * FROM ref_kabupaten ORDER BY rp_name")->result(), // i: CI query, data pada select option
			"html_option_value"		: "id_ref_provinsi", // i: value dari query yang akan dijadikan base value untuk kolom ini
			"html_option_text"		: ["id_ref_provinsi","rp_name"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)
			"html_option_multiple"	; true, //multiple selection
			"js_cascade_field"		: "ref_provinsi_id", // i: nama field lain yang akan mempengaruhi selection field ini
			"js_cascade_col"		: "ref_provinsi_id", // i: FK . nama attribut di field ini yang akan dipengaruhi oleh isian field lain
			"custom_id"				: "sel_lokasi", //i: id field, jika null, id_field akan menggunakan db_field_name
			"custom_class"			: "class_select", //i: jika ingin menambahkan kelas pada field ini
			"custom_js"				: "$('#sel_lokasi').on('change',function(console.log('done')))", //i: additional javascript
		},
	];
 */ 


//============================================ PLEASE CHANGE THIS SETTINGS ========================================
class User extends CI_Controller {
//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

	function __construct()
	{
		parent::__construct();
		$this->auth->isAdmin();

		$this->FORM = $this->form();
		date_default_timezone_set('Asia/Jakarta');


		//============================================ PLEASE CHANGE THIS SETTINGS ========================================
		$this->main_table = "user";
		$this->primary_key = "id_user";
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================


		
	}

	private function form(){
		$pt_id = $this->session->userdata('up');
		//============================================ PLEASE CHANGE THIS SETTINGS ========================================
		$field = array(

			[
				"db_field_name" 		=> "pt_id",
				"html_required"			=> true,
				"html_field_name"		=> "Perusahaan",
				"html_type_input" 		=> "select",
				"html_options"			=> $this->db->query("SELECT * FROM pt WHERE is_active = 1 ORDER BY pt_name")->result(), 
				"html_option_value"		=> "id_pt", // i: value dari query yang akan dijadikan base value untuk kolom ini
				"html_option_text"		=> ["pt_name"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)
				"html_init_value"		=> $pt_id,
			],
			[
				"db_field_name" 		=> "u_nik",
				"html_required"			=> true,
				"html_field_name"		=> "NIK",
				"html_placeholder"		=> "Nomor Induk Karyawan",
				"html_type_input" 		=> "text",	
				"html_field_note"		=> "Maksimal 30 karakter",
				"html_max"				=> 30,		
			],
			[
				"db_field_name" 		=> "u_name",
				"html_required"			=> true,
				"html_field_name"		=> "Nama Karyawan",
				"html_placeholder"		=> "Nama Karyawan",
				"html_type_input" 		=> "text",			
			],
			[
				"db_field_name" 		=> "u_phone",
				"html_required"			=> false,
				"html_field_name"		=> "Nomor HP",
				"html_type_input" 		=> "text",			
			],
			[
				"db_field_name" 		=> "u_level",
				"html_required"			=> true,
				"html_field_name"		=> "Level karyawan",
				"html_type_input" 		=> "select",
				"html_options"		    => array(array("level" => "Anggota"), array("level" => "Danru"), array("level" => "Chief"), array("level" => "Manajemen"), array("level" => "Officer"), array("level" => "Admin")),	
				"html_option_multiple"	=> false,	
				"html_option_value"		=> "level",
				"html_option_text"		=> ['level'],	
			],
            [
				"db_field_name" 		=> "u_password",
				"html_required"			=> true,
				"html_field_name"		=> "Password",
				"html_type_input" 		=> "password",
				"custom_class"			=> "flash-password",	
				"html_field_note"		=> '<span class="form-text text-muted hide" id="flashing" style="cursor: pointer;">Tampilkan password <i class="fas fa-eye"></i></span>',		
			],

		);
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

		return $field;
	}




	public function index()
	{

		$updateid= $this->encryption->decrypt($this->input->post('id'));
		$datatable = array();
		if($updateid){
			$getval = (array) $this->db->query("SELECT * FROM $this->main_table WHERE $this->primary_key = $updateid")->result();
			$getval = (array) $getval[0];
			foreach ($this->FORM as $key => $value) {
				$this->FORM[$key]['html_init_value'] = $getval[$this->FORM[$key]['db_field_name']];
			}
		}
		$HTMLDATA['form'] = $this->FORM;
		$HTMLDATA['selected_id'] = $updateid;
		$HTMLDATA['datatable'] = &$datatable;



		//============================================ PLEASE CHANGE THIS SETTINGS ========================================

		if(!$this->input->get('id')){
			$up = $this->session->userdata('up');
			$datatable = $this->db->query("SELECT id_user, u_nik as NIK, u_name as Nama, u_level as Level, pt.pt_name as Perusahaan, u_phone as 'Nomor HP'
											FROM user
											LEFT JOIN pt ON pt.id_pt=user.pt_id
											WHERE user.is_active = 1 
											AND (user.pt_id = $up OR user.u_level = 'Officer')
											ORDER BY id_user DESC");
		}

		$pt = $this->session->userdata('up');
		$pt_name = $this->db->query("SELECT pt_name FROM pt WHERE id_pt = $pt");
		$pt_name = $pt_name->row()->pt_name;

		// HTML INFORMATION
		$HTMLDATA['pt']  				= $this->db->query("SELECT * FROM pt WHERE is_active = 1 ORDER BY pt_name DESC ");
		$HTMLDATA['pt_name']  = $pt_name;
		$HTMLDATA["controller_name"] 	= "user"; // nama controller untuk URL
		$HTMLDATA["main_menu"] 			= "data"; // main menu yang dibuka/aktif
		$HTMLDATA["sub_menu"] 			= "user";	// sub menu yang aktif

		// DATATABLE SETTING
		$HTMLDATA["datatable_id"] 		= "id_user"; // primary key data, pastikan sama dengan query diatas
		$HTMLDATA["is_id_hide"] 		= true; // kalau id tidak ingin ditampilkan di datatable, set True
		
		
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

		

		$this->load->view("data/v_data_management",$HTMLDATA);
	}


	
	public function delete(){
		$updateid= $this->encryption->decrypt($this->input->post('id'));
		if($updateid){
			$data =array(
				"is_active" => 0
			);
			$update = $this->simple->update($this->main_table,$data,$this->primary_key,$updateid);
			$this->session->set_flashdata('info', 'Data berhasil dihapus');
		}
		redirect('data/'.$this->main_table);
	}


	public function input(){
		$id = $this->encryption->decrypt($this->input->post('id'));
		$form = $this->form();
		$data = array();
		
		foreach ($form as $f) {
			$data[$f['db_field_name']] = $this->input->post($f['db_field_name']);
		}

		//============ IF YOU NEED TO CHANGE THE VALUE MANUALLY =======
			/* IF YOU NEED TO CHANGE THE VALUE MANUALLY
				print_r($data); //kalau mau ngecek data yang bakal diinputin ke DB, 
				$data['rk_name'] = "ganti nama";  //kalau misalnya mau manual nimpah data
			*/
		//============ END - IF YOU NEED TO CHANGE THE VALUE MANUALLY =======
		$data['u_password'] = $this->encryption->encrypt($data['u_password']);

		if(!$id){ //INSERT NEW ROW	
			$data['created_at']	= date("Y-m-d H:i:s");
			$data['is_active']	= 1;
			$nik = $data['u_nik'];
			$cek_nik = $this->db->query("SELECT id_user, u_nik FROM user WHERE u_nik = '$nik' ");
			if($cek_nik->num_rows()){
				$this->session->set_flashdata('danger', 'Data gagal dimasukkan, NIK sudah digunakan oleh user lain');
				redirect('data/'.$this->main_table);
			}
			$insert = $this->simple->insert($this->main_table,$data);
			if(isset($insert['code'])){
				$this->session->set_flashdata('danger', 'Data gagal dimasukkan, '.$insert['message']);
			}else{
				$this->session->set_flashdata('success', 'Data baru berhasil dimasukkan');
			}
			redirect('data/'.$this->main_table);

		}else{ // UPDATE EXISTING ROW
			$nik = $data['u_nik'];
			$cek_nik = $this->db->query("SELECT id_user, u_nik FROM user WHERE u_nik = '$nik' ");
			if($cek_nik->num_rows()){
				if($id != $cek_nik->row()->id_user){
					$this->session->set_flashdata('danger', 'Data gagal diubah, NIK sudah digunakan oleh user lain');
					redirect('data/'.$this->main_table);
				}
			}

			$update = $this->simple->update($this->main_table,$data,$this->primary_key,$id);
			if(isset($update['code'])){
				$this->session->set_flashdata('danger', 'Data gagal diubah, '.$update['message']);
			}else{
				$this->session->set_flashdata('success', 'Data berhasil diperbaharui');
			}
			redirect('data/'.$this->main_table);
		}
		
	}

	public function change_pt(){
		$pt_id= $this->input->post('pt_id');
		$menu= $this->input->post('sub_menu');

		$pt_name = $this->db->query("SELECT pt_name FROM pt where id_pt = $pt_id");
        $pt_name = $pt_name->row()->pt_name;
        
		$data = array(
			'up' => $pt_id,
			'upn' => $pt_name
		);
		$this->session->set_userdata($data);

		$id_user = $this->encryption->decrypt($this->session->userdata("uid"));
		$data=array(
			'pt_id' => $pt_id,
		);
		$this->simple->update('user',$data,'id_user',$id_user);
	}


}
