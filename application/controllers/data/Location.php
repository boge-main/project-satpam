<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/* FORM INFORMATION 
	$field = [
		{
			"db_field_name" 		: "kabupaten", // i: untuk dijadikan field id, harus menggunakan nama field di db, pastikan harus unik
			"html_field_name"		: "Kabupaten",  // i: akan dijadikan nama kolom isian (nama field di HTML)
			"html_readonly"			: true, untuk matiin input
			"html_init_value"		: "3101", // i: untuk set default value di kolom isian
			"html_type_input" 		: "select", //value: text|number|select|textarea|date|email| = https://www.w3schools.com/tags/tag_input.asp
			"html_placeholder"		: "Please select an option", // i: placeholder (text before input)
			"html_field_note"		: "please ensure your province is correct", // i: untuk memberikan catatan kaki pada field form
			"html_max"				: null, // i: max value, berlaku untuk field date|number
			"html_min"				: null, // i: min value, berlaku untuk field date|number
			"html_required"			: true|false,
			"html_options"			: $this->db->query("SELECT * FROM ref_kabupaten ORDER BY rp_name")->result(), // i: CI query, data pada select option
			"html_option_value"		: "id_ref_provinsi", // i: value dari query yang akan dijadikan base value untuk kolom ini
			"html_option_text"		: ["id_ref_provinsi","rp_name"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)
			"html_option_multiple"	; true, //multiple selection
			"js_cascade_field"		: "ref_provinsi_id", // i: nama field lain yang akan mempengaruhi selection field ini
			"js_cascade_col"		: "ref_provinsi_id", // i: FK . nama attribut di field ini yang akan dipengaruhi oleh isian field lain
			"custom_id"				: "sel_lokasi", //i: id field, jika null, id_field akan menggunakan db_field_name
			"custom_class"			: "class_select", //i: jika ingin menambahkan kelas pada field ini
			"custom_js"				: "$('#sel_lokasi').on('change',function(console.log('done')))", //i: additional javascript
		},
	];
 */ 


//============================================ PLEASE CHANGE THIS SETTINGS ========================================
class Location extends CI_Controller {
//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

	function __construct()
	{
		parent::__construct();
		$this->auth->isAdmin();

		$this->FORM = $this->form();
		date_default_timezone_set('Asia/Jakarta');



		//============================================ PLEASE CHANGE THIS SETTINGS ========================================
		$this->main_table = "location";
		$this->primary_key = "id_location";
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================


		
	}

	private function form(){
		$pt_id = $this->session->userdata('up');
		//============================================ PLEASE CHANGE THIS SETTINGS ========================================
		$field = array(
            [
				"db_field_name" 		=> "pt_id",
				"html_required"			=> true,
				"html_field_name"		=> "Perusahaan",
				"html_type_input" 		=> "select",
				"html_options"			=> $this->db->query("SELECT * FROM pt WHERE is_active = 1 AND id_pt = $pt_id ORDER BY pt_name")->result(), 
				"html_option_value"		=> "id_pt", // i: value dari query yang akan dijadikan base value untuk kolom ini
				"html_option_text"		=> ["pt_name"], // i: text yang akan ditampilkan pada option select (max 30 char per attribute)
				"html_init_value"		=> $pt_id,
			],
			[
				"db_field_name" 		=> "location_name",
				"html_required"			=> true,
				"html_field_name"		=> "Nama lokasi",
				"html_placeholder"		=> "isikan dengan nama lokasi",
				"html_type_input" 		=> "text",			
			],
			[
				"db_field_name" 		=> "location_desc",
				"html_required"			=> true,
				"html_field_name"		=> "Deskripsi",
				"html_type_input" 		=> "textarea",			
			],
            [
				"db_field_name" 		=> "location_qr_code",
				"html_required"			=> true,
				"html_field_name"		=> "Qr_code",
				"html_type_input" 		=> "text",
                "html_readonly"			=> false, 
			    "html_init_value"		=> uniqid().uniqid().uniqid(), // i: untuk set default value di kolom isian	
			    "html_field_note"		=> "*Qr code hasil generate otomatis oleh sistem dan akan menghasilkan QR yang dapat discan. apabila ingin merubah QR code, dapat memasukkan random kode secara bebas. Semakin panjang kode QR, akan semakin rumit pembacaan kode pada kamera HP. disarankan menggunakan maksimal 50 digit/karakter kode", 

			],

		);
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

		return $field;
	}

	public function index()
	{

		$updateid= $this->encryption->decrypt($this->input->post('id'));
		$datatable = array();
		if($updateid){
			$getval = (array) $this->db->query("SELECT * FROM $this->main_table WHERE $this->primary_key = $updateid")->result();
			$getval = (array) $getval[0];
			foreach ($this->FORM as $key => $value) {
				$this->FORM[$key]['html_init_value'] = $getval[$this->FORM[$key]['db_field_name']];
			}
		}
		$HTMLDATA['form'] = $this->FORM;
		$HTMLDATA['selected_id'] = $updateid;
		$HTMLDATA['datatable'] = &$datatable;



		//============================================ PLEASE CHANGE THIS SETTINGS ========================================

		if(!$this->input->get('id')){
			$up = $this->session->userdata('up');
			$datatable = $this->db->query("SELECT id_location, 
											pt.pt_name as 'Perusahaan',  
											location_name as 'Nama Titik Patroli'

											FROM location
											LEFT JOIN pt ON location.pt_id = pt.id_pt 
											WHERE pt_id = $up
											ORDER BY id_location DESC");
		}


		$pt = $this->session->userdata('up');
		$pt_name = $this->db->query("SELECT pt_name FROM pt WHERE id_pt = $pt");
		$pt_name = $pt_name->row()->pt_name;

		// HTML INFORMATION
		$HTMLDATA['pt']  				= $this->db->query("SELECT * FROM pt WHERE is_active = 1 ORDER BY pt_name DESC ");
		$HTMLDATA['pt_name']  			= $pt_name;
		$HTMLDATA["controller_name"] 	= "location"; // nama controller untuk URL
		$HTMLDATA["main_menu"] 			= "data"; // main menu yang dibuka/aktif
		$HTMLDATA["sub_menu"] 			= "location";	// sub menu yang aktif

		// DATATABLE SETTING
		$HTMLDATA["datatable_id"] 		= "id_location"; // primary key data, pastikan sama dengan query diatas
		$HTMLDATA["is_id_hide"] 		= true; // kalau id tidak ingin ditampilkan di datatable, set True
		
		
		//============================================ END - PLEASE CHANGE THIS SETTINGS ========================================

		

		$this->load->view("data/v_data_management",$HTMLDATA);
	}





	public function qrcode(){
 		
 		$updateid= $this->encryption->decrypt($this->input->post('id'));
 		$rebuild= $this->input->post('rebuild');

		if($updateid){
			$get = $this->db->query("SELECT id_location, location_qr_code, location_name, pt.id_pt,  pt.pt_name  
									FROM location
									LEFT JOIN pt ON location.pt_id = pt.id_pt 
									WHERE id_location = $updateid");

			if($rebuild == 1){
	 			$text = $get->row()->location_qr_code;
				$this->create_qr($text);
	 		}
			$data =array(
				'location' => $get,
				"controller_name" 	=> "location" // nama controller untuk URL
			);
			$this->load->view("data/get_qr_code.php",$data);

		}else{
			"<h1>It Works</h1>";
		}
	}



	private function create_qr($text){
		if(file_exists('./assets/qr/images/'.$text.'.png')){
        	unlink('./assets/qr/images/'.$text.'.png');
        }
		$this->load->library('ciqrcode'); //pemanggilan library QR CODE
		$config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/qr/cache/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/qr/error/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/qr/images/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
 		

        $image_name= $text.'.png'; //buat name dari qr code sesuai dengan kode generate
 
        $params['data'] = $text; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
	}


	
	public function delete(){
		$updateid= $this->encryption->decrypt($this->input->post('id'));
		if($updateid){
			$data =array(
				"is_active" => 0
			);
			$update = $this->simple->update($this->main_table,$data,$this->primary_key,$updateid);
			$this->session->set_flashdata('info', 'Data berhasil dihapus');
		}
		redirect('data/'.$this->main_table);
	}

	public function input(){
		$id = $this->encryption->decrypt($this->input->post('id'));
		$form = $this->form();
		$data = array();
		foreach ($form as $f) {
			$data[$f['db_field_name']] = $this->input->post($f['db_field_name']);
			$data['created_at']	= date("Y-m-d H:i:s");
			$data['is_active']	= 1;
		}
		//============ IF YOU NEED TO CHANGE THE VALUE MANUALLY =======
			/* IF YOU NEED TO CHANGE THE VALUE MANUALLY
				print_r($data); //kalau mau ngecek data yang bakal diinputin ke DB, 
				$data['rk_name'] = "ganti nama";  //kalau misalnya mau manual nimpah data
			*/
		//============ END - IF YOU NEED TO CHANGE THE VALUE MANUALLY =======
		
		$this->create_qr($data['location_qr_code']);

		if(!$id){ //INSERT NEW ROW	
			$insert = $this->simple->insert($this->main_table,$data);
			if(isset($insert['code'])){
				$this->session->set_flashdata('danger', 'Data gagal dimasukkan, '.$insert['message']);
			}else{
				$this->session->set_flashdata('success', 'Data baru berhasil dimasukkan');
			}
			redirect('data/'.$this->main_table);

		}else{ // UPDATE EXISTING ROW
			$update = $this->simple->update($this->main_table,$data,$this->primary_key,$id);
			if(isset($update['code'])){
				$this->session->set_flashdata('danger', 'Data gagal diubah, '.$update['message']);
			}else{
				$this->session->set_flashdata('success', 'Data berhasil diperbaharui');
			}
			redirect('data/'.$this->main_table);
		}
		
	}

	public function change_pt(){
		$pt_id= $this->input->post('pt_id');
		$menu= $this->input->post('sub_menu');

		$pt_name = $this->db->query("SELECT pt_name FROM pt where id_pt = $pt_id");
        $pt_name = $pt_name->row()->pt_name;
        
		$data = array(
			'up' => $pt_id,
			'upn' => $pt_name
		);
		$this->session->set_userdata($data);

		$id_user = $this->encryption->decrypt($this->session->userdata("uid"));
		$data=array(
			'pt_id' => $pt_id,
		);
		$this->simple->update('user',$data,'id_user',$id_user);
	}


}
