<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Scanqr extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->auth->isLogin();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index(){
		$getkey = $this->input->post("value");
		$getimage = $this->input->post("scan");
		$location = $this->db->query("SELECT * FROM location WHERE location_qr_code = '$getkey' AND is_active = 1" );

		if($location->num_rows() == 1){
			if($location->row()->pt_id == $this->session->userdata('up')){
				$data=array(
					'record_date_time' 	=> date("Y-m-d H:i:s"),
					'pt_id' 			=> $location->row()->pt_id,
					'location_id' 		=> $location->row()->id_location,
					'user_id'			=> $this->encryption->decrypt($this->session->userdata('uid')),
					'created_at' 		=> date("Y-m-d H:i:s"),
					'is_active'			=> 1
				);
				$insert = $this->simple->insert('record',$data);
				$dtimg = array(
					"record_id" => $insert,
					"image"		=> $getimage
				);
				$insert_img = $this->simple->insert('scanned_image',$dtimg);
				
				if(isset($insert['code'])){
					$this->session->set_flashdata('danger', 'There is something wrong, please contact your admin. [Err 143.01]');
					echo $this->auth->goToPageReturn();
				}else{
					$this->session->set_flashdata('success', 'Pemeriksaan lokasi berhasil direkam di sistem');
					echo $this->auth->goToPageReturn();
				}
			}else{
				$this->session->set_flashdata('danger', 'Anda tidak memiliki akses ke perusahaan terkait, silahkan hubungi admin!');
			}
			
		}else{
			$this->session->set_flashdata('danger', 'There is something wrong, please contact your admin. [Err 143.02]');
			echo $this->auth->goToPageReturn();
		}
	}

	public function getimages(){
		$id_record = $this->input->post("idrecord");
		$sel = $this->db->query("SELECT * FROM pictures WHERE record_id = $id_record");
		print_r(json_encode($sel->result()));
	}

	public function saveimage(){
		$img = [0,0,0,0,0];
		$img[1] = $this->input->post('img_satu');
		$img[2] = $this->input->post('img_dua');
		$img[3] = $this->input->post('img_tiga');
		$img[4] = $this->input->post('img_empat');
		$id_record = $this->input->post('id_record');
		$isada = 0;


		$sel = $this->db->query("SELECT * FROM pictures WHERE record_id = $id_record");
		if($sel->num_rows()){
			foreach ($sel->result() as $row) {
				$this->simple->delete('pictures','id_pictures',$row->id_pictures);
			}
		}

		foreach ($img as $key => $value) {
			if($key > 0 && $value){
				echo $value.'<br/>';
				$imgs = str_replace('data:image/png;base64,', '', $value);
				$imgs = str_replace(' ', '+', $imgs);
				$data = base64_decode($imgs);
				$name = $id_record."-".$key.".png";
				$dtdb['img'.$key] = $name;
				$file = "./assets/pictures/".$name;
				$success = file_put_contents($file, $data);
				$this->session->set_flashdata('success', 'Foto Dokumentasi berhasil disimpan.');
				$isada = 1;
			}
		}

		$dtdb = array(
			'record_id'	=> $id_record
		);

		for($i=1;$i<=4;$i++){
			$name = $id_record.'-'.$i.'.png';
			$filename = './assets/pictures/'.$name;
			if (file_exists($filename)) {
			    $dtdb['img'.$i] = $name;
			} else {
			    echo "The file $filename does not exist";
			}
		}
		$insert_img = $this->simple->insert('pictures',$dtdb);
		if($isada == 0){
			$this->session->set_flashdata('danger', 'Tidak ada foto dokumentasi baru yang disimpan.');
		}
		
	}


}
