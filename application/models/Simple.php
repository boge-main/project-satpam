<?php
class Simple extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
	
	
	function view($table,$order,$sort){
		$this->db->order_by($order,$sort);
		return $this->db->get($table);
	}
	function view_by_and_order($table,$by,$val,$order,$sort){
		$this->db->where($by,$val);
		$this->db->order_by($order,$sort);
		return $this->db->get($table);
	}
	function view_by_and_order_not_delete($table,$by,$val,$order,$sort){
		$this->db->where($by,$val);
		$this->db->where('deleted','0');
		$this->db->order_by($order,$sort);
		return $this->db->get($table);
	}
	
	function view_first($table,$order,$sort){
		$this->db->order_by($order,$sort);
		return $this->db->get($table,1);
	}
	
	function view_by($table,$by,$val){
		$this->db->where($by,$val);
		return $this->db->get($table);
	}
	function view_by_array_sort($table,$by,$val,$order,$sort){
		$this->db->where_in($by,$val);
		$this->db->order_by($order,$sort);
		return $this->db->get($table);
	}
	function view_by2($table,$by1,$val1,$by2,$val2){
		$this->db->where($by1,$val1);
		$this->db->where($by2,$val2);
		return $this->db->get($table);
	}
	
	function insert($table,$data){
		$ret = $this->db->insert($table,$data);
		if ($ret){
			return $this->db->insert_id();
		}else{
			return $this->db->error();
		}
	}
	function insert_batch($table,$data){
		$this->db->insert_batch($table, $data);
	}

	function update($table,$data,$id,$val)
	{
		$this->db->where($id,$val);
		$this->db->update($table,$data);
	}
	
	function delete($table,$id,$val)
	{
		$this->db->where($id,$val);
		$this->db->delete($table);
	}

}
?>