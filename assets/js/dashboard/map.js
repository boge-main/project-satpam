let CENTROID = {};
let map = 'map defined';

var CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
  subdomains: 'abcd',
  maxZoom: 20
});
let stadia_dark = L.tileLayer(
  "https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png",
  {
    opacity: 0.8,
    maxZoom: 20,
  }
);
var Stadia_Outdoors = L.tileLayer('https://tiles.stadiamaps.com/tiles/outdoors/{z}/{x}/{y}{r}.png', {
  maxZoom: 20,
  attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
});

let baseMaps = {
  "Dark": stadia_dark,
  "Light": CartoDB_Positron,
  "Simple": Stadia_Outdoors
};

var CartoDB_PositronOnlyLabels = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}{r}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
  subdomains: 'abcd',
  pane: 'labels',
  maxZoom: 20
});


$(document).ready(function(){
  
  $("#collapse-button-filter").on('click',function(){
    setTimeout(function(){
      if($('#collapseExample').is(":visible")){
        $("#iconfilter-collapse").attr("class","fas fa-chevron-circle-up");
      }else{
        $("#iconfilter-collapse").attr("class","fas fa-chevron-circle-down");
      }
    },1000);
    
  })

  $("#map").css("height",$("#box-map-summary").height()-15);

  map = L.map("map", {
    center: [-2.1833826,118.4902853],
    zoom: 4.9,
    zoomDelta: 0.1,
    zoomSnap: 0.1,
    zoomControl: 1,
    layerControl: 1,
    attributionControl: !1,
    minZoom: 4.9,
    maxZoom: 19,
    doubleClickZoom: 1,
    scrollWheelZoom: 1,
    preferCanvas: !1,
    // layers: [CartoDB_PositronOnlyLabels],
  });
  map.zoomControl.setPosition('bottomleft');
  // L.control.layers(baseMaps).addTo(map);

  d3.json(BASE_URL+"assets/geojson/indonesia-prov.geojson").then(function(statesData){
    map.createPane('labels');
    map.getPane('labels').style.zIndex = 500;
    map.getPane('labels').style.pointerEvents = 'none';
    CartoDB_PositronOnlyLabels.addTo(map);

    function getColor() {
      return "#e5e5f9";
    }
    function style(feature) {
      return {
        weight: 1,
        opacity: 1,
        color: '#fff',
        className: "geojson-path",
        dashArray: 0,
        fillOpacity: 1,
        fillColor: getColor()
      };
    }
    var geojson;
    geojson = L.geoJson(statesData, {
      style: style
    }).addTo(map);

    geojson.eachLayer(function (layer) {
        layer.bindPopup(SC.titleCase(layer.feature.properties.Propinsi));
    });
    map.fitBounds(geojson.getBounds());
    map.setMaxBounds(geojson.getBounds());

    d3.json(BASE_URL+"assets/geojson/indonesia-district.geojson").then(function(statesData){
      var geojson;
      geojson = L.geoJson(statesData, {});
      geojson.eachLayer(function (layer) {
          CENTROID[layer.feature.properties.IDKABUPATE] = layer.getBounds().getCenter();
      });
      call_page_api(); //calling one page API in main.js
    });
  })

  
})

function put_map_information(data){
  if(map.hasLayer(ALL_SVG['GROUP_MARKERS'])){
    map.removeLayer(ALL_SVG['GROUP_MARKERS']);
  }
  ALL_SVG['GROUP_MARKERS'] = L.markerClusterGroup();
  let t_prov = []
  let t_kab =[]
  let t_kegiatan = 0
  let t_budget = 0;
  data.forEach(function(d){
    ALL_SVG['GROUP_MARKERS'].addLayer(L.marker([CENTROID[d.ref_kabkot_id].lat,CENTROID[d.ref_kabkot_id].lng]).bindPopup("<b>"+d['rk_name']+", "+d['rp_name']+"</b><br/>"+d['k_kegiatan_kunci'])  );
    t_kegiatan +=1;
    t_budget += parseInt(d['k_budget']);
    t_prov.push(d['ref_provinsi_id']);
    t_kab.push(d['ref_kabkot_id']);
  })
  t_prov = [...new Set(t_prov)].length;
  t_kab = [...new Set(t_kab)].length;
  $("#t_prov").html(SC.humnum(t_prov));
  $("#t_kab").html(SC.humnum(t_kab));
  $("#t_kegiatan").html(SC.humnum(t_kegiatan));
  $("#t_budget").html(SC.humnum(t_budget));
  map.addLayer(ALL_SVG['GROUP_MARKERS']);
}





