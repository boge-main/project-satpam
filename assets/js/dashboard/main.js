const SC = {
  "lfnumber" : d3.format(".2s"),
  "humnum" : d3.format(",.0f"),
  "titleCase": (str)=>{
          return str.toLowerCase().replace(/\b(\w)/g, s => s.toUpperCase());
  },
};
let ALL_SVG = [];

function call_page_api(json_api = BASE_URL+"dashboard/main_dashboard/one_page_api"){
	d3.json(json_api).then(function(data){
		put_map_information(data.peta); //call function to draw markers in map.js
		draw_piechart_output(data.output); //call function to draw chart in chart.js
		draw_barchart_sumber_dana(data.sumber_dana); //call function to draw chart in chart.js
		draw_barchart_mitra_pelaksana(data.mitra_pelaksana); //call function to draw chart in chart.js
		draw_multibar_budget(data.output_quartal_budget) //call function to draw chart in chart.js
		draw_chart_pelaksana(data.pelaksana_kegiatan) //call function to draw chart in chart.js
		updateDataTables(data.datatables);
	})

}