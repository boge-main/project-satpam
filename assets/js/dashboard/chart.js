function draw_piechart_output(data){
  let series = [];
  let labels = [];
  data.forEach(function(d){
    series.push(parseInt(d.total));
    labels.push(d.ro_desc+" ("+d.ro_kode+")");
  })
  var options = {
    // series: [25, 15, 44, 55, 41, 17],
    series: series,
    chart: {
      width: '100%',
      type: 'donut',
    },
    // labels: ["Output 1", "Output 2", "Output 3", "Output 4", "Output 5", "Output 6"],
    labels:labels,
    theme: {
        mode: 'light', 
        monochrome: {
            enabled: true,
            color: '#4d4cac',
            shadeTo: 'light',
            shadeIntensity: 0.65
        },
    },
    plotOptions: {
      pie: {
        dataLabels: {
          offset: -5
        }
      }
    },
    dataLabels: {
      formatter(val, opts) {
        // const name = opts.w.globals.labels[opts.seriesIndex]
        return [val.toFixed(0) + '%']
      }
    },
    tooltip: {
      enabled: true,
      y: {
        formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
          return SC.humnum(value)
        }
      }
    },
    legend: {
      show: true,
      position: 'bottom',
      horizontalAlign: 'left', 
      fontSize: '8px',
      itemMargin: {
          vertical: -2
      },
      markers: {
          radius: 10,
      },
    }
  };

  var chart = new ApexCharts(document.querySelector("#piechart-output"), options);
  chart.render();

}

function draw_barchart_sumber_dana(data){
  let series = [];
  let labels = [];
  data.forEach(function(d){
    series.push(parseInt(d.total));
    labels.push(d.rsd_name);
  })
  var options = {
    series: [{
      name : 'Dana',
      // data: [1100000000,1000000000,900000000,800000000,700000000,600000000,500000000,400000000,300000000,200000000,100000000],
      data: series,
      colors:["#fff"]
    }],
    chart: {
      type: 'bar',
      height: '85%',
      width:'98%',
      toolbar: {
        show: false,
      },
    },
    plotOptions: {
      bar: {
        borderRadius: 4,
        horizontal: true,
      }
    },
    colors: "#ff808b" ,
    dataLabels: {
      enabled: false
    },
    tooltip: {
      enabled: true,
      y: {
        formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
          return "Rp "+SC.humnum(value)
        }
      }
    },
    yaxis:{
      labels: {
          align: 'right',
      },
    },
    xaxis: {
      // categories: ['SK90', 'CND32', 'UK890', 'NTH12', 'ITL13', 'FR34', 'JPN43','US123', 'CHN43', 'GRM1D'],
      categories: labels,
      labels: {
          align: 'right',
          formatter: (value) => {return SC.lfnumber(value) },
      },
    },
    grid: {
      show: false,   
    },
  };

  var chart = new ApexCharts(document.querySelector("#barchart-sumber-dana"), options);
  chart.render();
}

function draw_barchart_mitra_pelaksana(data){
  let series = [];
  let labels = [];
  data.forEach(function(d){
    series.push(parseInt(d.total));
    labels.push(d.rip_name);
  })
  var options = {
    series: [{
      name : 'Kegiatan',
      // data: [68, 53, 40, 22, 10, 5],
      data: series,
      colors:["#fff"]
    }],
    chart: {
      type: 'bar',
      height: '85%',
      width:'98%',
      toolbar: {
        show: false,
      },
    },
    plotOptions: {
      bar: {
        borderRadius: 4,
        horizontal: true,
      }
    },
    colors: "#c8c9e9" ,
    dataLabels: {
      enabled: false
    },
    yaxis:{
      labels: {
          align: 'right',
      },
    },
    xaxis: {
      // categories: ['Bappenas', 'Kemenkes', 'BPS', 'Kemendagri', 'FKM UI', 'BKKBN'],
      categories:labels,
      labels: {
          align: 'right',
          formatter: (value) => { return SC.lfnumber(value) },
      },
    },
    tooltip: {
      enabled: true,
      y: {
        formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
          return SC.humnum(value)
        }
      }
    },
    grid: {
      show: false,   
    },
  };

  var chart = new ApexCharts(document.querySelector("#barchart-mitra-pelaksana"), options);
  chart.render();
}

function draw_multibar_budget(data){
  
  // let series = [
  //   {
  //     name: 'Output 1',
  //     data: [159, 123, 331, 100]
  //   }, 
  //   {
  //     name: 'Output 2',
  //     data: [123, 355, 141, 164]
  //   }, 
  //   {
  //     name: 'Output 3',
  //     data: [244, 155, 341, 464]
  //   }, 
  //   {
  //     name: 'Output 4',
  //     data: [144, 355, 241, 264]
  //   }, 
  // ];

  let series = [];
  
  data[0].forEach(function(d,i){
    series[i] = {"name":d};
  })
  data[1].forEach(function(d,i){
    series[i]['data']=d;
  })

  var options = {
    series: series,
      chart: {
      type: 'bar',
      height: '85%',
      width:'98%',
      toolbar: {
        show: false,
      },
    },
    plotOptions: {
      bar: {
        horizontal: false,
        dataLabels: {
          position: 'top',
        },
      }
    },
    colors: ["#5e81f4",'#4d4cac','#9698d6','#ff808b'], 
    dataLabels: {
      enabled: false,
      offsetX: 0,
      style: {
        fontSize: '12px',
        colors: ['#fff']
      }
    },
    stroke: {
      show: true,
      width: 1,
      colors: ['#fff']
    },
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
          return "Rp "+SC.humnum(value)
        }
      }
    },
    xaxis: {
      categories: ['Quartal 1', 'Quartal 2', 'Quartal 3', 'Quartal 4'],
    },
    legend: {
      show: true,
      position: 'bottom',
      horizontalAlign: 'left', 
      fontSize: '8px',
      itemMargin: {
          vertical: -2
      },
      markers: {
          radius: 10,
      },
    }
  };

  var chart = new ApexCharts(document.querySelector("#multibar-budget"), options);
  chart.render();

}

function draw_chart_pelaksana(data){
  let series = [];
  let labels = [];
  data.forEach(function(d){
    series.push(parseInt(d.total));
    labels.push(d.rdn_name);
  })
  var options = {
    // series: [25, 15,],
    series:series,
    chart: {
      width: '100%',
      type: 'donut',
    },
    // labels: ["DEX", "NEX",],
    labels:labels,
    theme: {
        mode: 'light', 
        monochrome: {
            enabled: true,
            color: '#7895f7',
            shadeTo: 'light',
            shadeIntensity: 0.65
        },
    },
    plotOptions: {
      pie: {
        dataLabels: {
          offset: -5
        }
      }
    },
    dataLabels: {
      formatter(val, opts) {
        // const name = opts.w.globals.labels[opts.seriesIndex]
        return [val.toFixed(1) + '%']
      }
    },
    tooltip: {
      enabled: true,
      y: {
        formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
          return SC.humnum(value)
        }
      }
    },
    legend: {
      show: true,
      position: 'bottom',
      labels: {
          useSeriesColors: false
      },
      markers: {
          width: 12,
          height: 12,
          strokeWidth: 0,
          strokeColor: '#fff',
          fillColors: undefined,
          radius: 12,
          customHTML: undefined,
          onClick: undefined,
          offsetX: 0,
          offsetY: 0
      },
    }
  };

  var chart = new ApexCharts(document.querySelector("#chart-pelaksana"), options);
  chart.render();
}


